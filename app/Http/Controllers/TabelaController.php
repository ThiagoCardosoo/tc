<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

/*tcardoso*/
class TabelaController extends Controller
{
      public function __construct()
      {
        $this->middleware('auth');
      }

    //lista dados da tabela Aluno
    public function listaJsonAluno(){
    	 $tablesAluno = ['Aluno'];

    	 foreach($tablesAluno as $a){
              $table_info_columnsAluno = DB::select( DB::raw('SHOW COLUMNS FROM '.$a));
        }
        echo json_encode($table_info_columnsAluno);
	}

  //lista dados da tabela Disciplina
  public function listaJsonDisciplina(){
       $tablesDisciplina = ['Disciplina'];

       foreach($tablesDisciplina as $a){
              $table_info_columnsDisciplina = DB::select( DB::raw('SHOW COLUMNS FROM '.$a));
        }
        echo json_encode($table_info_columnsDisciplina);
  }

  //lista dados da tabela Turma
   public function listaJsonTurma(){
       $tablesTurma = ['Turma'];

       foreach($tablesTurma as $a){
              $table_info_columnsTurma= DB::select( DB::raw('SHOW COLUMNS FROM '.$a));
        }
        echo json_encode($table_info_columnsTurma);
  }

  //lista dados da tabela HistoricoEscolar
   public function listaJsonHistoricoEscolar(){
       $tablesHistoricoEscolar = ['HistoricoEscolar'];

       foreach($tablesHistoricoEscolar as $a){
              $table_info_columnsHistoricoEscolar = DB::select( DB::raw('SHOW COLUMNS FROM '.$a));
        }
        echo json_encode($table_info_columnsHistoricoEscolar);
  }

  //lista dados da tabela PreRequisito
   public function listaJsonPreRequisito(){
       $tablesPreRequisito = ['PreRequisito'];

       foreach($tablesPreRequisito as $a){
              $table_info_columnsPreRequisito = DB::select( DB::raw('SHOW COLUMNS FROM '.$a));
        }
        echo json_encode($table_info_columnsPreRequisito);
  }

  //mostra a consulta basica uma tabela somente
 	public function listaJsonResultado($query){

        if (strpos($query, 'WHERE') !== false && strlen($query) < 50) {
        $stringAux = explode("=", $query);
        $queryResult = $stringAux[0]." = '".trim($stringAux[1])."'";
        $results = DB::select( DB::raw("$queryResult") );
        echo json_encode($results);

        //se for maior de 50 significa que colocou o inner join junto com o where
      }else if(strpos($query, 'WHERE')){

        $stringAux = explode("WHERE", $query);
        $tabela = explode("=",$stringAux[1] );
        $resultadowhere = "'".trim($tabela[1])."'";
        $query = $stringAux[0]."WHERE".$tabela[0]."=".$resultadowhere;
        $results = DB::select( DB::raw("$query") );
        echo json_encode($results);

      }else if(strlen($query) == 123){

        $stringAux = explode("WHERE", $query);
        $tabela = explode("=",$stringAux[1] );
        $resultadowhere = "'".trim($tabela[1])."'";
        $query = $stringAux[0]."WHERE".$tabela[0]."=".$resultadowhere;
        $results = DB::select( DB::raw("$query") );
        echo json_encode($results);
      }else{
        $results = DB::select( DB::raw("$query") );
        echo json_encode($results);
      }
      exit();
  	}

    //mostra a o resultado da simulacao(inner join)
     public function listaJsonResultadoSimulacao($query, $pk=null){
         if (strpos($query, 'INNER JOIN') == true && strpos($query, 'ON') == false) {
             $results = DB::select( DB::raw("$query"));
               echo json_encode($results);

            //juncao com where e * innerjoin
         } else if (strpos($query, 'INNER JOIN') == false && strpos($query, 'WHERE') == true && strpos($query, '*') == true
       && strpos($query, ',') == true) {

           $results = DB::select( DB::raw("$query"));
             echo json_encode($results);

         }else if (strpos($query, 'INNER JOIN') == false && strpos($query, 'WHERE') == true) {

           $results = DB::select( DB::raw("$query"));
             echo json_encode($results);

         }else{
            $stringAux = explode("=", $query);
            $queryResult = $stringAux[0];
            $result = $query." WHERE ".$stringAux[1]."=".$pk." LIMIT 1";
            //exit();
            $results = DB::select( DB::raw("$result") );
            //echo json_encode($results);
          }
          exit();
    	}


      //mostra os dados da lista com sql individual com inner join - tabela esquerda
      public function listaJsonResultadoSimulacaojoinIndividualEsquerda($query, $tabelacount1, $tabelacount2, $tabela1, $tabela2, $pk=null){

      if (strpos($query, 'INNER JOIN') == true && strpos($query, '*') == false) {

      $campos = explode(',',preg_replace('/SELECT | FROM.*/','',$query));
      $camposTabela1 = $camposTabela2 = '';
      foreach($campos as $campo){
        if (strpos(trim($campo),$tabela1)===0) {
          $camposTabela1 .= $campo . ',';
        }else{
          $camposTabela2 .= $campo . ',';
        }
      }
      $camposTabela1 = substr($camposTabela1, 0, -1);
      $camposTabela2 = substr($camposTabela2, 0, -1);

       $tabelaesq = "SELECT ".$camposTabela1." FROM ".$tabela1;

         $tabelaux =  explode("INNER", $query);
         $stringAux = explode("=", $query);
         $stringAuxCampo =  explode(".", $stringAux[1]);
         $resultEsquerda = $tabelaesq." WHERE ".$stringAuxCampo[1]."=".$pk." LIMIT 1";

          if($pk != null)
          {
            $resultadoTabelaEsquerda = DB::select( DB::raw($resultEsquerda) );;
            echo json_encode($resultadoTabelaEsquerda);
          }else{
            if(strpos($query, 'WHERE'))
            {
               $tabelaesq = "SELECT ".$camposTabela1." FROM ".$tabela1;
               $resultadoTabelaEsquerda = DB::select( DB::raw($tabelaesq) );;
               echo json_encode($resultadoTabelaEsquerda);

              if(isset($partes[2]))
              {
                  if(strpos($query, 'INNER'))
                  {

                      $stringAux1 = explode("FROM", $partes[0]);
                      $stringAux2 = explode("FROM", $partes[1]);

                      $tabelaesq =  explode("INNER", $query);
                      $stringAuxFrom = explode("FROM", $tabelaesq[0]);


                      $resultEsquerda = $stringAux1[0].",".$stringAux2[0]."FROM ".$stringAuxFrom[1];

                      $resultadoTabelaEsquerda = DB::select( DB::raw($resultEsquerda) );
                      echo json_encode($resultadoTabelaEsquerda);

                  }else{

                  $stringAux = explode("FROM", $partes[2]);
                  $tabelaesq =  explode("INNER", $stringAux[1]);
                  $stringWhere =  explode("WHERE", $stringAux[1]);
                  $resultEsquerda = $partes[0].",".$partes[1]."FROM ".$tabelaesq[0];

                  $resultadoTabelaEsquerda = DB::select( DB::raw($resultEsquerda) );
                  echo json_encode($resultadoTabelaEsquerda);
                }
              }
            }else{

              $resultadoTabelaEsquerda = DB::select( DB::raw($tabelaesq) );
              echo json_encode($resultadoTabelaEsquerda);
            }
          }
        exit();
       }
     }


      // mostra os dados da lista com sql individual com inner join - tabela direita
       public function listaJsonResultadoSimulacaojoinIndividualDireita($query, $tabelacount1, $tabelacount2, $tabela1, $tabela2, $pk=null){

       if (strpos($query, 'INNER JOIN') == true && strpos($query, '*') == false) {

         $campos = explode(',',preg_replace('/SELECT | FROM.*/','',$query));
         $camposTabela1 = $camposTabela2 = '';
         foreach($campos as $campo){
           if (strpos(trim($campo),$tabela1)===0) {
             $camposTabela1 .= $campo . ',';
           }else{
             $camposTabela2 .= $campo . ',';
           }
         }
         $camposTabela1 = substr($camposTabela1, 0, -1);
         $camposTabela2 = substr($camposTabela2, 0, -1);

        $tabeladir = "SELECT ".$camposTabela2." FROM ".$tabela2;

         $tabelaux =  explode("INNER", $query);
         $stringAux = explode("=", $query);
         $stringAuxCampo =  explode(".", $stringAux[1]);
         $resultDireita = $tabeladir." WHERE ".$stringAuxCampo[1]."=".$pk." LIMIT 1";

           if($pk != null)
           {
             $resultadoTabelaDireita = DB::select( DB::raw($resultDireita));;
             echo json_encode($resultadoTabelaDireita);
           }else{
             $resultadoTabelaDireita = DB::select( DB::raw($tabeladir));;
             echo json_encode($resultadoTabelaDireita);
           }
         exit();
        }
      }


      //mostra os dados da lista utilizando where tabela esquerda
      public function listaJsonResultadoTabelaEsquerdaWhere($query, $pk=null, $where=null){

            $tabelaux =  explode("INNER", $query);
            $stringAux = explode("=", $query);

            $stringAuxPK = explode("ON", $stringAux[0]);
            $stringAuxCampo =  explode(".", $stringAux[1]);
            $tabelaesquerda =  explode("FROM", $tabelaux[0]);
            $stringEsquerda = "SELECT * FROM ".$tabelaesquerda[1]."";

            //separo para pegar a condicao do where
            $whereresult =  explode("=", $where);
            $whereresultCondicao =  explode(".", $where);

            $stringDireita = "SELECT * FROM ".$whereresultCondicao[0]."";

            //verifico se foi colocado a primary key no where
            if(strlen($whereresult[1]) > 2)
            {
                 $result = $stringEsquerda." WHERE ".$whereresult[0]." = '".trim($whereresult[1])."' AND ".$stringAuxPK[1]." = '".trim($pk)."' LIMIT 1";
                 $resultadoTabelaEsquerda = DB::select( DB::raw($result) );;
                 echo json_encode($resultadoTabelaEsquerda);
                 exit();
            }else{

              //verifico se a linha que esta sendo comparada esta no resultado do where
              if($pk == $whereresult[1]){

                //verifico se a tabela esquerda é igual a tabela.campo where
                if(strpos($tabelaesquerda[1],$whereresultCondicao[0])){

                  $result = $stringEsquerda." WHERE ".$whereresult[0]." = '".trim($whereresult[1])."' LIMIT 1";
                  $resultadoTabelaEsquerda = DB::select( DB::raw($result) );;
                   echo json_encode($resultadoTabelaEsquerda);
                    exit();
                }else{

                     $result = $stringDireita." WHERE ".$whereresult[0]." = '".trim($whereresult[1])."' LIMIT 1";
                     $resultadoTabelaEsquerda = DB::select( DB::raw($result) );;
                     echo json_encode($resultadoTabelaEsquerda);
                     exit();
                }

              }else{
                exit();
              }
            }
          }


      //mostra os dados da lista resultado simulacao join campos separados com where tabela esquerda
      public function listaJsonResultadoSimulacaojoinIndividualEsquerdaWhere($query, $tabela1, $tabela2, $pk=null,$where=null){
          //pego a tabela junto com o from
          preg_match('/(?<=FROM )(\w+)/', $query, $tabelafinal1, PREG_OFFSET_CAPTURE);
          //pego a tabela colocada no inner join
          preg_match('/(?<=INNER JOIN )(\w+)/', $query, $tabelafinal2, PREG_OFFSET_CAPTURE);
          $tabelafinal1 = $tabelafinal1[0][0];
          $tabelafinal2 = $tabelafinal2[0][0];
          $campos = explode(',',preg_replace('/SELECT | FROM.*/','',$query));
          $camposTabela1 = $camposTabela2 = '';

          foreach($campos as $campo){
            if (strpos(trim($campo),$tabelafinal1)===0) {
              $camposTabela1 .= $campo . ',';
            }else{
              $camposTabela2 .= $campo . ',';
            }
          }
          $camposTabela1 = substr($camposTabela1, 0, -1);
          $camposTabela2 = substr($camposTabela2, 0, -1);


            $tabelaesq = "SELECT ".$camposTabela1." FROM ".$tabelafinal1;
            $stringAux = explode("=", $query);
            $stringAuxPK = explode("ON", $stringAux[0]);
            $whereresult =  explode("=", $where);

            $stringInner = explode("INNER JOIN ", $query);

            $stringInner = explode("INNER JOIN ", $query);
            $stringInner2 = explode("WHERE ", $stringInner[1]);

            $tabelaesq2 = "SELECT ".$camposTabela1." FROM ".$tabelafinal1." INNER JOIN ".$stringInner2[0];
          //  exit();

            if(strlen($whereresult[1]) > 2)
            {

                 $result = $tabelaesq2." WHERE ".$whereresult[0]." = '".trim($whereresult[1])."' AND ".$stringAuxPK[1]." = '".trim($pk)."'";
                 //exit();
            }else{

              //verifico se a linha que esta sendo comparada esta no resultado do where
                if($pk == $whereresult[1]){

                 $result = $tabelaesq2." WHERE ".$whereresult[0]." = '".trim($whereresult[1])."'";
                 //exit();
              }else{
                exit();
              }
            }
            //echo $result;
          //  exit();
            $resultadoTabelaEsquerda = DB::select( DB::raw($result) );;
             echo json_encode($resultadoTabelaEsquerda);
              exit();
         }


         // lista com sql individual com inner join e WHERE - tabela direita
         //mostra os dados da lista resultado simulacao join campos separados com where tabela direita
          public function listaJsonResultadoSimulacaojoinIndividualDireitaWhere($query, $tabela1, $tabela2, $pk=null,$where=null){
            //expressao regular para separar as 2 tabelas
            preg_match('/(?<=FROM )(\w+)/', $query, $tabelafinal1, PREG_OFFSET_CAPTURE);
            preg_match('/(?<=INNER JOIN )(\w+)/', $query, $tabelafinal2, PREG_OFFSET_CAPTURE);
            $tabelafinal1 = $tabelafinal1[0][0];
            $tabelafinal2 = $tabelafinal2[0][0];

            $campos = explode(',',preg_replace('/SELECT | FROM.*/','',$query));
            $camposTabela1 = $camposTabela2 = '';
            foreach($campos as $campo){
              if (strpos(trim($campo),$tabelafinal1)===0) {
                $camposTabela1 .= $campo . ',';
              }else{
                $camposTabela2 .= $campo . ',';
              }
            }
            $camposTabela1 = substr($camposTabela1, 0, -1);
            $camposTabela2 = substr($camposTabela2, 0, -1);


           $stringAux = explode("=", $query);
           $stringAuxPK = explode("ON", $stringAux[0]);
           $whereresult =  explode("=", $where);
           $stringAuxPKWhere =  explode(".", $stringAuxPK[1]);


           $whereresult =  explode("=", $where);
           $whereresultString =  explode(".", $whereresult[0]);
           $stringWhere = explode("=", $where);

           $stringInner = explode("INNER JOIN ", $query);
           $stringInner2 = explode("WHERE ", $stringInner[1]);

          $tabeladir2 = "SELECT ".$camposTabela2." FROM ".$tabelafinal1." INNER JOIN ".$stringInner2[0];
          //  exit();

           if(strlen($whereresult[1]) > 2)
             {

                  $result = $tabeladir2." WHERE ".$whereresult[0]." = '".trim($whereresult[1])."' AND ".$stringAuxPK[1]." = '".trim($pk)."'";
                //exit();
             }else{

               //verifico se a linha que esta sendo comparada esta no resultado do where
                 if($pk == $whereresult[1]){
                   $result = $tabeladir2." WHERE ".$whereresult[0]." = '".trim($whereresult[1])."' AND ".$stringAuxPK[1]." = '".trim($pk)."'";
               }else{
                 exit();
               }
             }

             $resultadoTabelaDireita = DB::select( DB::raw($result) );;

              echo json_encode($resultadoTabelaDireita);
               exit();

         }

       //mostra a lista dos dados tabela direita com where
      public function listaJsonResultadoTabelaDireitaWhere($query, $pk=null, $where=null){

         $stringWhere = explode("=", $where);
         $stringAuxPK = explode("INNER", $query);
         $stringAuxPK = explode("=", $stringAuxPK[1]);
         $stringAuxPK = explode("WHERE", $stringAuxPK[1]);
         $tabeladireita =  explode("JOIN", $query);
         $stringDireita = "SELECT * FROM ".$tabeladireita[1]."";
         $stringAux = explode("=", $query);
         $stringAuxCampo =  explode(".", $stringAux[1]);
         $stringDireitaAux =  explode("ON", $stringDireita);


         $verificaTabela = explode("INNER JOIN", $query);
         $verificaTabela = explode("FROM", $verificaTabela[0]);

         //separo para pegar a condicao do where
         $whereresult =  explode("=", $where);
         $whereresultCondicao =  explode(".", $where);

         $stringDireitaOther = "SELECT * FROM ".$verificaTabela[1]."";


         if(strlen($stringWhere[1]) > 2)
         {
                $result = $stringDireitaAux[0]." WHERE ".$stringAuxPK[0]." = '".trim($pk)."' LIMIT 1";
                $resultadoTabelaDireita = DB::select( DB::raw($result) );
                 echo json_encode($resultadoTabelaDireita);
                exit();
         }else{


           //verifico se a linha que esta sendo comparada esta no resultado do where
           if($pk == $whereresult[1]){

               if(strpos($verificaTabela[1],$whereresultCondicao[0])){

                 $result = $stringDireitaAux[0]." WHERE ".$stringAuxPK[0]." = '".trim($pk)."' LIMIT 1";

                 $resultabelaverificaigual =  explode("WHERE", $result);
                 $resultabelaverificaigual = explode("FROM", $resultabelaverificaigual[0]);

                 $resultwhere =  explode("WHERE", $result);
                 $resultwhere = explode(".", $resultwhere[1]);


                  //verifico se a tabela direita é igual tabela.campo
                  if(strpos($resultabelaverificaigual[1],$resultwhere[0])){
                      $resultadoTabelaDireita = DB::select( DB::raw($result) );
                     echo json_encode($resultadoTabelaDireita);
                     exit();
                  }else{


                     $result2 = $stringDireitaOther." WHERE ".$stringAuxPK[0]." = '".trim($pk)."' LIMIT 1";
                      $resultadoTabelaDireita = DB::select( DB::raw($result2) );
                     echo json_encode($resultadoTabelaDireita);
                     exit();
                  }

              }else{

                $result = $stringDireitaOther." WHERE ".$stringAuxPK[0]." = '".trim($pk)."' LIMIT 1";
                $result = preg_replace('/(?<=WHERE ).*[.]/','',$result);
                $resultadoTabelaDireita = DB::select( DB::raw($result) );
                echo json_encode($resultadoTabelaDireita);
                exit();
              }
           }else{

             exit();
           }
         }

         $resultadoTabelaDireita = DB::select( DB::raw($result) );
            echo json_encode($resultadoTabelaDireita);
             exit();
      }

    //mostra a lista dos dados tabela esquerda
    public function listaJsonResultadoTabelaEsquerda($query, $pk=null){

      //se nao adicionei o on significa que foi feito um produto cartesiano
      if (strpos($query, 'INNER JOIN') == true && strpos($query, 'ON') == false) {

        $tabelaux =  explode("INNER", $query);
        $tabelaesquerda =  explode("FROM", $tabelaux[0]);
        $stringEsquerda = "SELECT * FROM ".$tabelaesquerda[1]."";
        $resultadoTabelaEsquerda = DB::select( DB::raw($stringEsquerda));
        echo json_encode($resultadoTabelaEsquerda);

        //juncao com where e * innerjoin
      }else if (strpos($query, 'INNER JOIN') == false && strpos($query, 'WHERE') == true && strpos($query, '*') == true
      && strpos($query, ',') == true) {

        $stringEsquerda =  explode(",",$query);
        $resultadoTabelaEsquerda = DB::select( DB::raw($stringEsquerda[0]));
        echo json_encode($resultadoTabelaEsquerda);
        exit();

        //se coloco o where e não coloco o inner join na sintaxe
      }else if (strpos($query, 'INNER JOIN') == false && strpos($query, 'WHERE') == true) {

        $tabelaux =  explode("INNER", $query);
        $stringAux = explode("=", $query);
        $stringAuxCampo =  explode(".", $stringAux[1]);
        $tabelaesquerda =  explode("FROM", $tabelaux[0]);
        $stringEsquerda = "SELECT * FROM ".$tabelaesquerda[1]."";

        //verifico se foi adicionado a condicao do where
        if($pk != null){
            $result = $stringEsquerda." WHERE ".$stringAuxCampo[1]."=".$pk." LIMIT 1";
            $resultadoTabelaEsquerda = DB::select( DB::raw($result) );;
        }else{
            $resultadoTabelaEsquerda = DB::select( DB::raw($stringEsquerda) );;
        }
         echo json_encode($resultadoTabelaEsquerda);

        exit();

      }else{

        $tabelaux =  explode("INNER", $query);
        $stringAux = explode("=", $query);
        $stringAuxCampo =  explode(".", $stringAux[1]);
        $tabelaesquerda =  explode("FROM", $tabelaux[0]);
        $stringEsquerda = "SELECT * FROM ".$tabelaesquerda[1]."";

        //verifico se foi adicionado a condicao do where
        if($pk != null){
            $result = $stringEsquerda." WHERE ".$stringAuxCampo[1]."=".$pk." LIMIT 1";
            $resultadoTabelaEsquerda = DB::select( DB::raw($result) );;
        }else{
            $resultadoTabelaEsquerda = DB::select( DB::raw($stringEsquerda) );;
        }
         echo json_encode($resultadoTabelaEsquerda);
        }
          exit();
      }

        //mostra a lista dos dados tabela direita
      public function listaJsonResultadoTabelaDireita($query, $pk=null){
        if (strpos($query, 'INNER JOIN') == true && strpos($query, 'ON') == false) {
          $tabeladireita =  explode("JOIN", $query);
         $stringDireita = "SELECT * FROM ".$tabeladireita[1]."";
          $resultadoTabelaDireita = DB::select(DB::raw($stringDireita));
             echo json_encode($resultadoTabelaDireita);

        }else if (strpos($query, 'INNER JOIN') == false && strpos($query, 'WHERE') == true) {

            $tabeladireita =  explode("WHERE", $query);
            $tabeladireita = explode(",",$tabeladireita[0]);
            $stringAux = explode("=", $query);
            $stringAuxCampo =  explode(".", $stringAux[1]);

            if(isset($tabeladireita[1])){
            $stringDireita = "SELECT * FROM ".$tabeladireita[1]."";
            $result = $stringDireita;
            $resultadoTabelaDireita = DB::select( DB::raw($result) );
            echo json_encode($resultadoTabelaDireita);
            exit();
          }else{
            exit();
          }
        }else{
                $tabeladireita =  explode("JOIN", $query);
                $stringDireita = "SELECT * FROM ".$tabeladireita[1]."";
                $stringAux = explode("=", $query);
                $stringAuxCampo =  explode(".", $stringAux[1]);

                if($pk != null){
                  $stringDireitaAux =  explode("ON", $stringDireita);
                  $result = $stringDireitaAux[0]." WHERE ".$stringAuxCampo[1]."=".$pk." LIMIT 1";
                  $resultadoTabelaDireita = DB::select( DB::raw($result) );
                }else{

                    if (strpos($stringDireita, 'ON') !== false) {
                      $stringDireitaAux =  explode("ON", $stringDireita);
                      $resultadoTabelaDireita = DB::select( DB::raw($stringDireitaAux[0]) );
                    }else{
                      $resultadoTabelaDireita = DB::select( DB::raw($stringDireita) );
                    }
                 }
              echo json_encode($resultadoTabelaDireita);
             }
           exit();
        }

        //juncao com where  e * mostra a lista dos dados tabela esquerda juncao com where
         public function listaJsonResultadoTabelaEsquerdaJuncaoWhere($query, $pk=null){

          if (strpos($query, 'INNER JOIN') == false && strpos($query, 'WHERE') == true
           && strpos($query, '*') == true && strpos($query, ',') == true){

              $tabelaux =  explode(",", $query);
              $stringAux = explode("=", $query);
              $stringAuxCampo =  explode(".", $stringAux[1]);

              $stringEsquerda = $tabelaux[0]." WHERE ".$stringAuxCampo[1]."=".$pk." LIMIT 1";
              $resultadoTabelaEsquerda = DB::select( DB::raw($stringEsquerda));;
              echo json_encode($resultadoTabelaEsquerda);
              exit();
            }
        }

          //juncao com where  e * mostra a lista dos dados tabela direita juncao com where
        public function listaJsonResultadoTabelaDireitaJuncaoWhere($query, $pk=null){

          if (strpos($query, 'INNER JOIN') == false && strpos($query, 'WHERE') == true
           && strpos($query, '*') == true && strpos($query, ',') == true){

              $tabelaux =  explode(",", $query);
              $tabelauxDireita = explode("WHERE", $tabelaux[1]);
              $stringAux = explode("=", $query);
              $stringAuxCampo =  explode(".", $stringAux[1]);
              $stringDireita = "SELECT * FROM ".$tabelauxDireita[0]." WHERE ".$stringAuxCampo[1]."=".$pk." LIMIT 1";
              $resultadoTabelaDireita = DB::select( DB::raw($stringDireita));
              echo json_encode($resultadoTabelaDireita);
              exit();
            }
        }
}
