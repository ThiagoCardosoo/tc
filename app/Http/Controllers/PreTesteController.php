<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Session;
use Redirect;
use Validator;

class PreTesteController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
      $arrPreTeste = [];
      $arrPreTesteOpcoes = [];
      $arrPreTeste = DB::select('SELECT * FROM pre_teste ');
      $arrPreTesteOpcoes = DB::select('SELECT * FROM pre_teste_opcoes ');

      return view('preteste/index')
      ->with('arrPreTeste', $arrPreTeste)
      ->with('arrPreTesteOpcoes', $arrPreTesteOpcoes);
  }

  public function opcoes()
  {
      return view('preteste/opcoes');
  }

    public function Store(Request $request)
    {
        $CodQuestao = Input::get('CodQuestao');
        $Questao = Input::get('Questao');
        $array = array();

        $Questao1 = $Questao[1];
        $Questao2 = $Questao[2];
        $Questao3 = $Questao[3];
        $Questao4 = $Questao[4];
        $Questao5 = $Questao[5];
        $Questao6 = $Questao[6];
        $Questao7 = $Questao[7];
        $Questao8 = $Questao[8];
        $Questao9 = $Questao[9];
        $Questao10 = $Questao[10];

        $arrReturnResult1 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 1');
        $arrReturnResult2 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 2');
        $arrReturnResult3 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 3');
        $arrReturnResult4 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 4');
        $arrReturnResult5 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 5');
        $arrReturnResult6 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 6');
        $arrReturnResult7 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 7');
        $arrReturnResult8 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 8');
        $arrReturnResult9 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 9');
        $arrReturnResult10 = DB::select('SELECT * FROM pre_teste_resposta WHERE CodQuestao = 10');

        //contabiliza os acertos conforme a questão
        $acertos = 0;
        foreach ($arrReturnResult1 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao1 == $CodResposta)
            {
              $acertos++;
              $questaoResp1 = "Questão 1 - acertou<br/>";
            }else{
              $questaoResp1 = "Questão 1 - Errou<br/>";
            }
        }

        foreach ($arrReturnResult2 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao2 == $CodResposta)
            {
              $acertos++;
              $questaoResp2 = "Questão 2 - Acertou<br />";
            }else{
              $questaoResp2 =  "Questão 2 - Errou<br />";
            }
        }

        foreach ($arrReturnResult3 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao3 == $CodResposta)
            {
              $acertos++;
              $questaoResp3 = "Questão 3 - Acertou<br />";
            }else{
              $questaoResp3 = "Questão 3 - Errou<br/>";
            }
        }

        foreach ($arrReturnResult4 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao4 == $CodResposta)
            {
              $acertos++;
              $questaoResp4 = "Questão 4 - Acertou<br />";
            }else{
              $questaoResp4 = "Questão 4 - Errou<br/>";
            }
        }

        foreach ($arrReturnResult5 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao5 == $CodResposta)
            {
              $acertos++;
              $questaoResp5 = "Questão 5 - Acertou<br />";
            }else{
              $questaoResp5 = "Questão 5 - Errou<br />";
            }
        }

        foreach ($arrReturnResult6 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao6 == $CodResposta)
            {
              $acertos++;
              $questaoResp6 =  "Questão 6 - Acertou<br />";
            }else{
              $questaoResp6 = "Questão 6 - Errou<br />";
            }
        }


        foreach ($arrReturnResult7 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao7 == $CodResposta)
            {
              $acertos++;
              $questaoResp7 = "Questão 7 - Acertou<br />";
            }else{
              $questaoResp7 =  "Questão 7 - Errou<br/>";
            }
        }

        foreach ($arrReturnResult8 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao8 == $CodResposta)
            {
              $acertos++;
              $questaoResp8 = "Questão 8 - Acertou<br />";
            }else{
              $questaoResp8 = "Questão 8 - Errou<br/>";
            }
        }

        foreach ($arrReturnResult9 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao9 == $CodResposta)
            {
              $acertos++;
              $questaoResp9 = "Questão 9 - Acertou<br />";
            }else{
              $questaoResp9 = "Questão 9 - Errou<br />";
            }
        }

        foreach ($arrReturnResult10 as $key => $value) {
          $CodResposta = $value->CodResposta;

            if($Questao10 == $CodResposta)
            {
              $acertos++;
              $questaoResp10 = "Questão 10 - Acertou<br />";
            }else{
              $questaoResp10 = "Questão 10 - Errou<br />";
            }
        }

        $id = Auth::user()->id;

        if($acertos > 7)
        {
           $link = "Clique no botão abaixo para ir para os modulos, ou no botão para repetir o pré teste.<a href='modulos'><button type=\"button\" class=\"btn btn-success\">Seguir para os modulos</button></a><a href='preteste'><button type=\"button\" class=\"btn btn-primary\">Repetir pré-teste</button></a><br />";
           $resultado =  "<br />Você teve ".($acertos*10)." % de acertos, Parabéns agora você pode seguir para os Modulos e tambem para a Simulação se desejar.";
           $situacao = "A";
        }else{
           $link = " Porêm se desejar clique no botão abaixo para ir para os modulos, ou no botão para repetir o pré teste.<a href='modulos'><button type=\"button\" class=\"btn btn-success\">Seguir para os modulos</button></a><a href='preteste'><button type=\"button\" class=\"btn btn-primary\">Repetir pré-teste</button></a>";
           $resultado = "<br />Você teve ".($acertos*10)." % de acertos é recomendavel que você revise os conceitos dos Modulos.";
           $situacao = "R";
        }

        //update do preteste(indicao de se tem acesso ou não aos modulos)
        DB::table('users')
          ->where('id', $id)
           ->update(array('preteste' => 1, 'situacao' => $situacao,
            'nota' => $acertos)
         );

        Session::flash('question-message', '<b>Resposta do seu Teste</b> <br />');
        Session::flash('question-message1', $questaoResp1);
        Session::flash('question-message2', $questaoResp2);
        Session::flash('question-message3', $questaoResp3);
        Session::flash('question-message4', $questaoResp4);
        Session::flash('question-message5', $questaoResp5);
        Session::flash('question-message6', $questaoResp6);
        Session::flash('question-message7', $questaoResp7);
        Session::flash('question-message8', $questaoResp8);
        Session::flash('question-message9', $questaoResp9);
        Session::flash('question-message10', $questaoResp10);
        Session::flash('question-message11', $resultado);
        Session::flash('question-message12', $link);
        return Redirect::to('pretesteopcoes');

       exit();
    }
}
