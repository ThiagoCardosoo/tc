<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EstruturaController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
    }

    //mostra o resultado das tabelas
    public function index(){


		    $tablesAluno = ['Aluno'];
		    $tablesDisciplina = ['Disciplina'];
		    $tablesTurma = ['Turma'];
		    $tablesPreRequisito= ['PreRequisito'];
        $tablesHistorico= ['HistoricoEscolar'];
            foreach($tablesAluno as $a){
              $table_info_columnsAluno = DB::select( DB::raw('SHOW COLUMNS FROM '.$a));
            }
            foreach($tablesDisciplina as $d){
              $table_info_columnsDisciplina = DB::select( DB::raw('SHOW COLUMNS FROM '.$d));
            }
            foreach($tablesTurma as $t){
              $table_info_columnsTurma = DB::select( DB::raw('SHOW COLUMNS FROM '.$t));
            }
            foreach($tablesPreRequisito as $p){
              $table_info_columnstablesPreRequisito = DB::select( DB::raw('SHOW COLUMNS FROM '.$p));
            }
            foreach($tablesHistorico as $h){
              $table_info_columnstablesHistorico = DB::select( DB::raw('SHOW COLUMNS FROM '.$h));
            }
              return view('estrutura.index')->with('alunos', $table_info_columnsAluno)->with('turmas', $table_info_columnsTurma)->with('disciplinas', $table_info_columnsDisciplina)->with('prerequisitos', $table_info_columnstablesPreRequisito)->with('historicos', $table_info_columnstablesHistorico);
	}
}
