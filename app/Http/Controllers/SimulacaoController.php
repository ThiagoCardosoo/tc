<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exercicio;

class SimulacaoController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

    public function index()
    {

      /*utilizado para exercicios randomicos
      $arrExercicios = DB::table('exercicios')
              ->limit(1)
              ->inRandomOrder()
              ->select(DB::raw('idExercicio,exercicio'))
              ->get();*/

      /*exercicios com paginacao */
       $arrExercicios =  Exercicio::paginate(1);

        return view('simulacao/index')
        ->with('arrExercicios', $arrExercicios);
    }

}
