<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Redirect;

class ModuloController extends Controller
{
      public function __construct()
      {
        $this->middleware('auth');
      }

      public function index()
      {
        if(Auth::user()->preteste == 0){
          return Redirect::to('preteste');
        }else{
          return view('modulos/index');
        }
      }

      public function modulo1()
      {
        if(Auth::user()->preteste == 0){
          return Redirect::to('preteste');
        }else{
          return view('modulos/modulo1');
        }

      }

      public function modulo2()
      {
        if(Auth::user()->preteste == 0){
          return Redirect::to('preteste');
        }else{
          return view('modulos/modulo2');
        }
      }

      public function modulo3()
      {
          if(Auth::user()->preteste == 0){
            return Redirect::to('preteste');
          }else{
          return view('modulos/modulo3');
        }
      }

}
