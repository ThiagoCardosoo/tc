<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreTesteOpcoes extends Model
{
  protected $table = 'pre_teste_opcoes';
  public $timestamps = false;
  protected $fillable = ['CodPretesteOp','CodQuestao','OpcaoA','OpcaoB',
  'OpcaoC','OpcaoD'];
}
