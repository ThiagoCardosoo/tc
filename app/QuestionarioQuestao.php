<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionarioQuestao extends Model
{
  protected $table = 'questionario_questao';
  public $timestamps = false;
  protected $fillable = ['CodQuestao','descricao'];
}
