<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionarioResposta extends Model
{
  protected $table = 'questionario_respostaa';
  public $timestamps = false;
  protected $fillable = ['CodResposta','CodQuestao','iduser','data','descricaoResposta'];
}
