<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreTeste extends Model
{
    protected $table = 'pre_teste';
    public $timestamps = false;
    protected $fillable = ['CodPreteste','CodQuestao','descricao'];
}
