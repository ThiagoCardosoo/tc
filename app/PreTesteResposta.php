<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreTesteResposta extends Model
{
  protected $table = 'pre_teste_resposta';
  public $timestamps = false;
  protected $fillable = ['CodTesteResp','CodQuestao','CodResposta'];
}
