<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
     protected $table = 'Aluno';
     public $timestamps = false;

}
