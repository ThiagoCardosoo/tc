<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercicio extends Model
{
    protected $table = 'exercicios';
    public $timestamps = false;
    protected $fillable = ['idExercicio','exercicio','modulo'];
}
