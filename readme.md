Repositório - trabalho de conclusão de curso Ciência da Computação - UNISC - Objeto de Aprendizagem para o apoio ao ensino de SQL.

Foi utilizado no desenvolvimento do OA o Framework Laravel 5 (PHP) e o Framework Angular JS (Javascript).

Requisitos para funcionamento do OA:
Servidor Apache
PHP 5.6
Banco de dados Mysql.

Instalar o Xampp ou outro servidor com o apache.
Execução a partir do servidor do Laravel - ir até a pasta cmd e digitar php artisan serve.

Execução a partir do codigo do git:
Clonar o repositorio git clone  https://bitbucket.org/ThiagoCardosoo/tc.git
após o clone, via linha de comando e com o composer instalado digitar: composer update
assim vai configurar as libs necessarias para o funcionamento.

OBS: é necessario renomear o arquivo .env.example para .env somente

Banco de dados (Configuração)
definido a partir do arquivo database e env.

Banco de dados
O banco de dados esta dentro da pasta database, a ultima versão é a bd_oa que possue toda a estrutura do OA.

Thiago Cardoso

Acesso ao OA:
[http://208.68.36.124/SQLSimulator/public/](http://208.68.36.124/SQLSimulator/public/)