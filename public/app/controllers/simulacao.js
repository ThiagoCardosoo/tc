var App = angular.module('drag-and-drop', ['ngDragDrop']);
/*tcardoso*/
App.controller('oneCtrl', function($scope, $timeout , $http) {

  //Dados da tabela Aluno
  $scope.list3 = [];
  $http.get('./tabela/listaJsonAluno')
  .then(function (result) {

    $scope.list3 = result.data;
    angular.forEach($scope.list3, function(d,i){
      d.Simbolo="X"; //utilizado para controle dos erros o x significa que é um campo
      $scope.list3[i] = d;
    });

    $scope.list10 = result.data;
    $scope.list4 = [
    { 'Field': 'Aluno', 'drag': true, 'Simbolo' : "T", 'Referencia' : 'A' } //- cada tabela recebe uma referencia
    ];
  });

  $scope.listc3 = [
    { 'Field': '', 'drag': true }

  ];

  $scope.sqlQuery = [];
  var arraySelect = [];
  var arraySelectCampo = [];
  var arraySelectJoin = [];
  var arraySelectIndividual = [];
  var arraySelectIndividualJoin = [];
  var sqlQueryIndividualJoin = [];

  var arraySimboloAux = [];
  var arrayReferencia = [];

//vetores para controle durante os erros
  var sqlQuery = $scope.sqlQuery = ['A', 'B', 'C','T','I','X','E'];
  var sqlQueryCampo = $scope.sqlQuery = ['X', 'A', 'C','H','I','E','X'];
  var sqlQueryIndividual = $scope.sqlQuery = ['A', 'T', 'G','X','C','T','H'];
  var sqlQueryJoin = $scope.sqlQuery = ['A', 'B', 'C','I','T','D','T','G','X','E'];
  var sqlQueryIndividualJoin = $scope.sqlQuery = ['A', 'T', 'G','X','C','T','H','J','D','E','I'];

  //adiciono nos vetores
  angular.forEach(sqlQuery, function(element) {
    arraySelect.push(element);
  });

  angular.forEach(sqlQueryCampo, function(element) {
    arraySelectCampo.push(element);
  });

  angular.forEach(sqlQueryJoin, function(element) {
    arraySelectJoin.push(element);
  });

  angular.forEach(sqlQueryIndividual, function(element) {
    arraySelectIndividual.push(element);
  });

  angular.forEach(sqlQueryIndividualJoin, function(element) {
    arraySelectIndividualJoin.push(element);
  });


    //metodo utilizado para adicionar um novo bloco personalizado
    $scope.addRowPontoText = function(index){

    var fieldPontoText =  { 'Field': '', 'drag': true }
       if($scope.listc3.length <= index+1){
            $scope.listc3.splice(index+1,0,fieldPontoText);
        }
    };

    //metodo utilizado para deletar um bloco personalizado criado
    $scope.deleteRowPontoText = function($event,index){
    if($event.which == 1)
      $scope.listc3.splice(index,1);
    }


  //Dados da tabela Disciplina
  $scope.list6 = [];
  $http.get('./tabela/listaJsonDisciplina')
  .then(function (result) {

    //console.log(result);
    $scope.list6 = result.data;
    $scope.list12 = result.data;
    angular.forEach($scope.list6, function(d,i){
      d.Simbolo="X"; //utilizado para controle dos erros o x significa que é um campo
      $scope.list6[i] = d;
    });
    $scope.list5 = [
    { 'Field': 'Disciplina', 'drag': true,'Simbolo' : "T",'Referencia' : 'D' }
    ];
  });

    //Dados da tabela HistoricoEscolar
  $scope.list15 = [];
  $http.get('./tabela/listaJsonHistoricoEscolar')
  .then(function (result) {

    //console.log(result);
    $scope.list16 = result.data;
    angular.forEach($scope.list16, function(d,i){
      d.Simbolo="X"; //utilizado para controle dos erros o x significa que é um campo
      $scope.list16[i] = d;
    });
    $scope.list15 = [
    { 'Field': 'HistoricoEscolar', 'drag': true,'Simbolo' : "T", 'Referencia' : 'H' }
    ];
  });

  // Dados da tabela PreRequisito
  $scope.list17 = [];
  $http.get('./tabela/listaJsonPreRequisito')
  .then(function (result) {

    //console.log(result);
    $scope.list18 = result.data;
    angular.forEach($scope.list18, function(d,i){
      d.Simbolo="X"; //utilizado para controle dos erros o x significa que é um campo
      $scope.list18[i] = d;
    });
    $scope.list17 = [
    { 'Field': 'PreRequisito', 'drag': true,'Simbolo' : "T",'Referencia' : 'P' }
    ];
  });


  //Dados da tabela Turma
  $scope.list8 = [];
  $http.get('./tabela/listaJsonTurma')
  .then(function (result) {

   // console.log(result);
    $scope.list8 = result.data;
    angular.forEach($scope.list8, function(d,i){
      d.Simbolo="X";
      $scope.list8[i] = d;
    });
    $scope.list14 = result.data;
    $scope.list7 = [
    { 'Field': 'Turma', 'drag': true, 'Simbolo' : "T",'Referencia' : 'T' }
    ];
  });

  $scope.list1 = [];

  //componentes select,* etc
  $scope.list2 = [
    { 'Field': 'SELECT', 'drag': true, 'Simbolo' : "A" },
    { 'Field': '*', 'drag': true, 'Simbolo' : "B" },
    { 'Field': 'FROM', 'drag': true, 'Simbolo' : "C" },
    { 'Field': 'ON', 'drag': true, 'Simbolo' : "D" },
    { 'Field': '=', 'drag': true, 'Simbolo' : "E" },
    { 'Field': 'AND', 'drag': true , 'Simbolo' : "F" },
    { 'Field': '.', 'drag': true , 'Simbolo' : "G" },
    { 'Field': ',', 'drag': true , 'Simbolo' : "H" },
    { 'Field': 'WHERE', 'drag': true, 'Simbolo' : "I" },
    { 'Field': 'INNER JOIN', 'drag': true, 'Simbolo' : "J" }
  ];

 $scope.arraySimbolo = [];
 var arraySimbolo = [];

//metodo para alternar entre as cores
$scope.colorAlternate = function(row,index) {
  var col = $scope.query;
    if(col.match(/JOIN/) && col.match(/ON/)){
      col = col.split("ON")[1].split("=")[0].split(".")[1].trim();
      //console.log(col);
  //console.log(index);
//console.log(col,row,index);
  var cores = ['#7ee5a2;','#bcbcbc;','#00ff00']
  num = -1;
//  console.log(index);
  if(col== row)
  {
    num=index
  }
  return cores[num-1];
  }
}

var strResult;
//metodo utilizado ao arrastar inicio
  $scope.startCallback = function(event, ui, Field, itemf) {

     //adiciono no array de simbolo e de referencia
     angular.forEach(Field.Simbolo, function(element) {
       arraySimbolo.push(element);
     });

     angular.forEach(Field.Referencia, function(element) {
       arrayReferencia.push(element);
     });
  //   console.log(arrayReferencia);

     var arrRef = arrayReferencia;
     var counts = {};

     for(var i = 0; i< arrayReferencia.length; i++) {
         var num = arrRef[i];
         counts[num] = counts[num] ? counts[num]+1 : 1;
     }
     if(counts['A'] > 0 || counts['H'])
     {
       //console.log("aqui");
       $scope.tamanhoTabelaPrimaria =  counts['A'];
       $scope.tamanhoTabelaSecundaria =  counts['H'];
       $scope.tabelaPrimariaJoin = "Aluno";
       $scope.tabelaSecundariaJoin = "HistoricoEscolar";
     }
     if(counts['D'] > 0 || counts['T'])
     {

       $scope.tamanhoTabelaPrimaria =  counts['T'];
       $scope.tamanhoTabelaSecundaria =  counts['D'];
       $scope.tabelaPrimariaJoin = "Disciplina";
       $scope.tabelaSecundariaJoin = "Turma";
     }

     if(counts['D'] > 0 && counts['P'])
     {

       $scope.tamanhoTabelaPrimaria =  counts['D'];
       $scope.tamanhoTabelaSecundaria =  counts['P'];
       $scope.tabelaPrimariaJoin = "Disciplina";
       $scope.tabelaSecundariaJoin = "prerequisito";
     }
     if(counts['T'] > 0 && counts['H'])
     {

       $scope.tamanhoTabelaPrimaria =  counts['T'];
       $scope.tamanhoTabelaSecundaria =  counts['H'];
       $scope.tabelaPrimariaJoin = "Turma";
       $scope.tabelaSecundariaJoin = "HistoricoEscolar";
     }


      $("#btnExecutar").show();
      $("#btnSimulacao").show();
  };

  $scope.stopCallback = function(event, ui) {

  };

  $scope.dragCallback = function(event, ui) {
      $("#btnSimulacao").hide();

  };

  //metodo antes de largar utilizado para ordenar os componentes
  $scope.beforeDrope = function(event, ui) {
    return new Promise((resolve, reject)=>{
      if (angular.element(ui.draggable).hasClass('itemMenu')){
        reject();
      }else{
        resolve();
      }
    });
  }

$scope.stringAux = [];
$scope.string = "";
$scope.flgFrom = false;
$scope.qtdPalavras = 0;
$scope.query = "";

//metodo de drop
  $scope.dropCallback = function(event, ui,Field) {
      $scope.query = $scope.getQryAsString(ui.draggable.scope());
       $scope.queryDrag = [];
       var array_selectDrag = [];
       var sqlQuery = $scope.query;

       angular.forEach(sqlQuery, function(element) {
         array_selectDrag.push(element);
       });

      var col = $scope.query;


       //verifica se foi digitado o SELECT com o * - foi digitado a query individual
      if(arraySimbolo[0] == 'A' && arraySimbolo[1] == "T")
      {
         if(arraySimbolo.length > 2)
         {
            //sugestao e erros o controle é feito a partir dos vetores de arraySimbolo e arraySelect
            if(arraySimbolo[2] != arraySelectIndividual[2])
            {
                console.log("Você precisa por o .");
                $scope.alert = "Erro!";
                //$scope.alertTitle = "Erro e Sugestões!";
                selector();
                return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o .';
            }
         }
         if(arraySimbolo.length > 3)
         {

           if(arraySimbolo[3] != arraySelectIndividual[3])
           {
               console.log("Você precisa por um campo");
               $scope.alert = "Erro!";
               selector();
               return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por um campo';
           }

         }
          //se adiciono o from
          if(!col.match(/,/)){

             if(arraySimbolo.length > 4)
             {

               if(arraySimbolo[4] != arraySelectIndividual[4])
               {
                   console.log("Você precisa por o FROM ou ,");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o FROM ou ,';
               }
             }
             if(arraySimbolo.length > 5)
             {
               if(arraySimbolo[5] != arraySelectIndividual[5]){

                 console.log("Você precisa por a tabela");
                 $scope.alert = "Erro!";
                 selector();
                 return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por a tabela';
               }
             }
             if(arraySimbolo.length > 6)
             {
               if(arraySimbolo[6] != arraySelectIndividualJoin[7]){

                 console.log("Sugestão utilizar o WHERE ou o INNER JOIN");
                 $scope.alert = "Erro!";
                 $scope.alertTitle = "Erro e Sugestões!";
                 selector();
                 return document.getElementById('divForErrorQuery').innerHTML = 'Sugestão utilizar o where';
               }
             }
          }

      }else{

        //se foi usado o *
       if(arraySimbolo.length > 0)
       {
         if(arraySimbolo[0] != arraySelect[0])
         {
            console.log("você precisa iniciar com o select");
            //alert("aqui");
            selector();
            $scope.alert = "Erro!";
            return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa iniciar com o select';

         }
       }
       if(arraySimbolo.length > 1)
       {
           if(arraySimbolo[1] != arraySelect[1] && arraySimbolo[1] != 'T' && arraySimbolo[1] != "X")
           {
              console.log("você precisa por o * , um campo ou uma tabela");
              selector();
              $scope.alert = "Erro!";
              return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o * , um campo ou uma tabela.campo';
           }
       }
       //sugestao e erros
       if(arraySimbolo.length > 2){
         if(arraySimbolo[2] != arraySelect[2] && arraySimbolo[1] != 'T' && arraySimbolo[1] != 'X')
         {
            console.log("Você precisa por o FROM");
            selector();
            $scope.alert = "Erro!";
            return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o FROM';
         }

       }
       if(arraySimbolo.length > 3){
         if(arraySimbolo[3] != arraySelect[3] && arraySimbolo[3] != 'X')
         {
            console.log("você precisa por a tabela");
            selector();
            $scope.alert = "Erro!";
            return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por a tabela';
         }
       }
        //vrifica se foi feito um INNER JOIN com *
          if(col.match(/JOIN/)){
           if(arraySimbolo[4] != arraySelect[4]){

              if(arraySimbolo.length > 5)
              {
                if(arraySimbolo[5] != arraySelectJoin[4])
                {
                   console.log("você precisa por a tabela secundaria para a relação do Join");
                   selector();
                   $scope.alert = "Erro!";
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por a tabela secundaria para a relação do Join';
                }
              }
              if(arraySimbolo.length > 6)
              {
                if(arraySimbolo[6] != arraySelectJoin[5])
                {
                   console.log("você precisa por o ON");
                   selector();
                   $scope.alert = "Erro!";
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o ON';
                }
              }
              if(arraySimbolo.length > 7)
              {
                if(arraySimbolo[7] != arraySelectJoin[6])
                {
                   console.log("você precisa por a primeira tabela");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'você precisa por a primeira tabela';
                }
              }
              if(arraySimbolo.length > 8)
              {
                if(arraySimbolo[8] != arraySelectJoin[7])
                {
                    console.log("você precisa por o .");
                    $scope.alert = "Erro!";
                    selector();
                    return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o .';
                }
              }
              if(arraySimbolo.length > 9)
              {
                if(arraySimbolo[9] != arraySelectJoin[8])
                {
                   console.log("você precisa por o campo chave Primária");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o campo chave Primária';
                }
              }
              if(arraySimbolo.length > 10)
              {
                if(arraySimbolo[10] != arraySelectJoin[9])
                {
                   console.log("você precisa por o =");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o =';
                }
              }
              if(arraySimbolo.length > 11)
              {
                if(arraySimbolo[11] != arraySelectJoin[4])
                {
                   console.log("você precisa por a tabela secundaria");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por a tabela secundaria';
                }
              }
              if(arraySimbolo.length > 12)
              {
                if(arraySimbolo[12] != arraySelectJoin[7])
                {
                   console.log("você precisa por o .");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o .';
                }
              }
              if(arraySimbolo.length > 13)
              {
                if(arraySimbolo[13] != arraySelectJoin[8])
                {
                   console.log("você precisa por o campo chave estrangeira");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o campo chave estrangeira';
                }
              }
              if(arraySimbolo.length > 14)
              {
                if(arraySimbolo[14] != arraySelectJoin[3])
                {
                   console.log("Sugestão utilize o WHERE");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Sugestão utilize o WHERE';
                }
              }
              if(arraySimbolo.length > 15)
              {
                if(arraySimbolo[15] != arraySelectJoin[6])
                {
                   console.log("você precisa por a tabela");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por a tabela';
                }
              }
              if(arraySimbolo.length > 16)
              {
                if(arraySimbolo[16] != arraySelectJoin[7])
                {
                   console.log("você precisa por o .");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o .';
                }
              }
              if(arraySimbolo.length > 17)
              {
                if(arraySimbolo[17] != arraySelectJoin[8])
                {
                   console.log("você precisa por o campo");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o campo';
                }
              }
              if(arraySimbolo.length > 18)
              {
                if(arraySimbolo[18] != arraySelectJoin[9])
                {
                   console.log("você precisa por o =");
                   $scope.alert = "Erro!";
                   selector();
                   return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o =';
                }
              }

           }
            // se preencheu com o WHERE
         }else{
           if(arraySimbolo.length > 4){
             if(arraySimbolo[4] != arraySelect[4] && arraySimbolo[3] != 'X')
             {
               //console.log("aqui where");
                console.log("Sugestão utilize o Where");
              //  console.log("você precisa por a tabela");
                selector();
                $scope.alert = "Sugestão!";
                return document.getElementById('divForErrorQuery').innerHTML = 'Sugestão utilize o Where ou o INNER JOIN';
             }
           }
           if(arraySimbolo.length > 5){
             if(arraySimbolo[5] != arraySelect[5])
             {
                console.log("Você precisa por o campo");
                $scope.alert = "Erro!";
                selector();
                return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o campo';
             }
           }
           if(arraySimbolo.length > 6){
             if(arraySimbolo[6] != arraySelect[6])
             {
                console.log("Você precisa por o =");
                $scope.alert = "Erro!";
                selector();
                return document.getElementById('divForErrorQuery').innerHTML = 'Você precisa por o =';
             }
           }
         }
    }

  };

// metodo utilizado para mostrar o alert dos erros
function selector() {
  $(".alert").stop().fadeTo(1, 1).removeClass('hidden');
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(".alert").addClass('hidden');
    });
  }, 3500);
}


//metodo utilizado ao executar as consultas
$scope.clickHandler = function() {
   $("#tabelas_container").show();
   $("#tabela1").show();
   $("#tabela2").show();

  $scope.query = "";
  //alguns tratamentos
  angular.forEach(document.querySelectorAll('.caption div'), (d,i)=>{
    $scope.query =
    (( (jQuery.inArray( d.innerHTML, ["*",".",",","="] ) <0)
    && ((jQuery.inArray( $scope.query.slice(-1), ["*",".",",","="] ) <0))) ?
      $scope.query +" "+ d.innerHTML : $scope.query+d.innerHTML);

  });

  if($scope.query == "")
  {
    $('#tabelas_container').hide();
  }

  //verifico o parser se foi digitado correto o comando
    sqliteParser($scope.query, function (err, ast) {

        var count = 0;
        //mostra o erro
        if (err) {
          console.log("error");
          document.getElementById('divForError').innerHTML = err.message;
          $('#tabelas_container').hide();
          return;
        }else{
          document.getElementById('divForError').innerHTML = '';
        }
      //console.log(ast);
    });

    $http.get('tabela/listaJsonResultado/' + $scope.query).success(function (result) {

          var str = $scope.query;

          $("#btnSimulacao").show();


          // se nao digitou o inner join
          if(!str.match(/INNER/)){

          //  $scope.resultData = result;
            $scope.resultData4 = result;

            var tabelaIndividual = str.split("FROM");
            //se digitiou o where
            if(str.match(/WHERE/))
            {
            //  console.log("adicionei o where");
                var tabelaIndividualsemwhere = tabelaIndividual[1].split("WHERE");
                $scope.tabelaIndividual = "Tabela"+tabelaIndividualsemwhere[0];
            }else{
              $scope.tabelaEsquerda = "Tabela"+tabelaIndividual[1];
            }
            //console.log(tabelaIndividual[1]);
              $("#btnSimulacao").hide();

          }else{

              $scope.resultData = result;
          }

          //se digitou o inner join
          if(str.match(/JOIN/)){
              $http.get('tabela/listaJsonResultadoTabelaEsquerda/' + $scope.query).success(function (result2) {
              $http.get('tabela/listaJsonResultadoTabelaDireita/' + $scope.query).success(function (result3) {

                  //pego o nome da tabela a esquerda
                  var separa = str.split("INNER");
                  var separaTabelaEsquerda = separa[0].split("FROM");
                  var tabelaEsquerda = separaTabelaEsquerda[1];
                  $scope.tabelaEsquerda = "Tabela"+tabelaEsquerda;

                  //pego o nome da tabela a direita
                  var separaInner = str.split("JOIN");
                  var tabelaDireita = separaInner[1];
                  var separaTabelaDireita = tabelaDireita.split("ON");
                 $scope.tabelaDireita = "Tabela"+separaTabelaDireita[0];

                 $scope.resultInner = "Resultado INNER JOIN";

                  $("#btnSimulacao").show();
              });
              });
            }

          //$scope.resultData = result;
          var strJoin = $scope.query;
          var strIndividualJoin = $scope.query;


            $http.get('tabela/listaJsonResultadoTabelaEsquerda/' + $scope.query).success(function (result4) {
            $http.get('tabela/listaJsonResultadoTabelaDireita/' + $scope.query).success(function (result5) {


            //select com * INNER JOIN
            if(!str.match(/ON/)){

              $scope.resultData4 = result4;
              $scope.resultData5 = result5;
               $("#tabela1").show();
               $("#tabela2").show();
               $("#tabelas_container").show();
               $("#btnSimulacao").hide();
               //$scope.resultInner = "Resultado Produto Cartesiano";
            }

              //select individual com INNER JOIN e ,
            if(str.match(/ON/) && str.match(/,/)){
              //sql individual tabela individual esquerda
              $http.get('tabela/listaJsonResultadoSimulacaojoinIndividualEsquerda/' + $scope.query  + '/' + $scope.tamanhoTabelaPrimaria  + '/' + $scope.tamanhoTabelaSecundaria +'/'+ $scope.tabelaPrimariaJoin +'/'+ $scope.tabelaSecundariaJoin).success(function (resultJoinIndividualEsquerda) {
                //sql individual tabela indiviual direita
              $http.get('tabela/listaJsonResultadoSimulacaojoinIndividualDireita/' + $scope.query  + '/' + $scope.tamanhoTabelaPrimaria  + '/' + $scope.tamanhoTabelaSecundaria +'/'+ $scope.tabelaPrimariaJoin +'/'+ $scope.tabelaSecundariaJoin).success(function (resultJoinIndividualDireita) {

              $scope.resultData4 = resultJoinIndividualEsquerda;
              $scope.resultData5 = resultJoinIndividualDireita;
          });
        });
        //vefico se nao foi colocado o ON, se foi adicionado a , e adicionado o WHERE
        //sintaxe produto cartesiano com where
      }else if(!str.match(/ON/) && str.match(/,/) && str.match(/WHERE/)){

          $http.get('tabela/listaJsonResultadoSimulacao/' + $scope.query).success(function (resultSimulacao) {

              $scope.resultData = resultSimulacao;
              $("#btnSimulacao").show();
              $scope.resultInner = "Resultado INNER JOIN";

              str = str.split("WHERE")[0].trim();
              strJoin = str.split(",")[0].trim();
              strJoin = str.split("FROM")[1].trim();
              strJoinEsquerda = strJoin.split(",")[0].trim();
              strJoinDireita = strJoin.split(",")[1].trim();
              $scope.tabelaDireita = "Tabela "+strJoinDireita;
              $scope.tabelaEsquerda = "Tabela "+strJoinEsquerda;
          //  console.log(strJoin);

            });

            //select com * e inner join
           }else if(str.match(/ON/)){
                 $("#buttons").show();

                  $scope.resultData4 = result4;
                  $scope.resultData5 = result5;
                }else{
                      // produto cartesiano
                        //console.log("produto cartesiano");
                        $http.get('tabela/listaJsonResultadoSimulacao/' + $scope.query).success(function (result11) {
                        $scope.resultData9= result4;
                        $scope.resultData10 = result5;

                        $scope.resultData11 = result11;

                    });
                }
            });
        });
      });

    //  console.log($scope.query);
       $("#btnLogSQL").show();

  };

/*Exemplo pré definido junção 1*/
$scope.clickHandlerExample1 = function() {
      $("#tabelas_container").show();
     $("#tabela1").show();
     $("#tabela2").show();

      $scope.query ="SELECT * FROM Aluno INNER JOIN HistoricoEscolar ON Aluno.CodAluno = HistoricoEscolar.CodAluno";

      $http.get('tabela/listaJsonResultado/' + $scope.query).success(function (result) {

            var str = $scope.query;

            $("#btnSimulacao").show();

            $scope.resultData = result;

            if(str.match(/JOIN/)){
                $http.get('tabela/listaJsonResultadoTabelaEsquerda/' + $scope.query).success(function (result2) {
                $http.get('tabela/listaJsonResultadoTabelaDireita/' + $scope.query).success(function (result3) {

                    //pego o nome da tabela a esquerda
                    var separa = str.split("INNER");
                    var separaTabelaEsquerda = separa[0].split("FROM");
                    var tabelaEsquerda = separaTabelaEsquerda[1];
                    $scope.tabelaEsquerda = "Tabela"+tabelaEsquerda;

                    //pego o nome da tabela a direita
                    var separaInner = str.split("JOIN");
                    var tabelaDireita = separaInner[1];
                    var separaTabelaDireita = tabelaDireita.split("ON");
                   $scope.tabelaDireita = "Tabela"+separaTabelaDireita[0];

                   $scope.resultInner = "Resultado INNER JOIN";

                    $("#btnSimulacao").show();
                });
                });
              }

              $http.get('tabela/listaJsonResultadoTabelaEsquerda/' + $scope.query).success(function (result4) {
              $http.get('tabela/listaJsonResultadoTabelaDireita/' + $scope.query).success(function (result5) {


               if(str.match(/ON/)){
                   $("#buttons").show();
                    $scope.resultData4 = result4;
                    $scope.resultData5 = result5;
                  }
              });
              });
        });
    };

  /*Exemplo pré definido junção 2 com where*/
  $scope.clickHandlerExample2 = function() {
    $("#tabelas_container").show();
   $("#tabela1").show();
   $("#tabela2").show();

    $scope.query = "SELECT * FROM Disciplina INNER JOIN Turma ON Disciplina.CodDisciplina = Turma.CodDisciplina WHERE Disciplina.NomeDisciplina = Algoritmo";

    $http.get('tabela/listaJsonResultado/' + $scope.query).success(function (result) {

          var str = $scope.query;

          $("#btnSimulacao").show();

          $scope.resultData = result;

          if(str.match(/JOIN/)){
              $http.get('tabela/listaJsonResultadoTabelaEsquerda/' + $scope.query).success(function (result2) {
              $http.get('tabela/listaJsonResultadoTabelaDireita/' + $scope.query).success(function (result3) {

                  //pego o nome da tabela a esquerda
                  var separa = str.split("INNER");
                  var separaTabelaEsquerda = separa[0].split("FROM");
                  var tabelaEsquerda = separaTabelaEsquerda[1];
                  $scope.tabelaEsquerda = "Tabela"+tabelaEsquerda;

                  //pego o nome da tabela a direita
                  var separaInner = str.split("JOIN");
                  var tabelaDireita = separaInner[1];
                  var separaTabelaDireita = tabelaDireita.split("ON");
                 $scope.tabelaDireita = "Tabela"+separaTabelaDireita[0];

                 $scope.resultInner = "Resultado INNER JOIN";

                  $("#btnSimulacao").show();
              });
              });
            }

            $http.get('tabela/listaJsonResultadoTabelaEsquerda/' + $scope.query).success(function (result4) {
            $http.get('tabela/listaJsonResultadoTabelaDireita/' + $scope.query).success(function (result5) {


             if(str.match(/ON/)){
                 $("#buttons").show();
                  $scope.resultData4 = result4;
                  $scope.resultData5 = result5;
                }
            });
            });
      });
  };

//metodo ao presionar o botao de simulacao
$scope.btnSimulation = function() {
   $("#tabelasimula").show();
   //varre tr a tr para a comparacao das linhas
  angular.forEach(document.querySelectorAll('#tabela1 tbody tr'), (d,i) => {
   $scope.tds.push({'first':{'ind':i}});
  angular.forEach(document.querySelectorAll('#tabela2 tbody tr'), (x,y) => {
    $scope.tds.push({'second':{'ind':y}});
  });
  });
  if (!$scope.simulando){
    $("#goback").show();
    $("#advance").show();
    $("#immediate").show();
  }
}

$scope.simulando = false;

//metodo se presionado o botao de simulacao automatico
$scope.imediateSimulation = function() {
  if (!$scope.simulando){
    $("#goback").hide();
    $("#advance").hide();
    $("#immediate").hide();
    angular.forEach(document.querySelectorAll('#tabela1 tbody tr'), (i,d) => {
      window.setTimeout(()=>{
        i.click(), angular.forEach(document.querySelectorAll('#tabela2 tbody tr'), (x,y) => {
          window.setTimeout(()=>{
            x.click();
            if (y == document.querySelectorAll('#tabela2 tbody tr').length-1 && d == document.querySelectorAll('#tabela1 tbody tr').length-1){
              $scope.simulando = false;
              $("#goback").show();
              $("#advance").show();
              $("#immediate").show();
            }
          }, y*3000);
        })}, d*(document.querySelectorAll('#tabela2 tbody tr').length)*3000);
    });
    $scope.simulando = true;
  }
}


$scope.step=0;
$scope.tds = [];

//metodo ao presionar o botao passo a passo da simulacao(step by step)
$scope.stepUp = function()
{
  ///$scope.tds[$scope.step].click();
if($scope.tds.length > $scope.step){
  if ($scope.tds[$scope.step]['first']){
    $scope.setClickedRow($scope.tds[$scope.step]['first']['ind']);
  }else{
    $scope.setClickedRowSecond($scope.tds[$scope.step]['second']['ind']);
  }
  }
  if ($scope.tds.length > $scope.step+1){
    $scope.step++;
  }
}

//metodo ao presionar o botao passo a passo da simulacao (step by step)
$scope.stepDown = function()
{

    if ($scope.tds[$scope.step]['first']){
      $scope.setClickedRow($scope.tds[$scope.step]['first']['ind']);
    }else{
      $scope.setClickedRowSecond($scope.tds[$scope.step]['second']['ind']);
    }
    if ($scope.step){
      $scope.step--;
    }
}

//metodo ao clicar numa linha da tabela
 $scope.setClickedRow = function(index) {
   $scope.selectedRow = index;
 }

//metodo para a comparacao das linhas algotimo Nested Loop
 $scope.setClickedRowSecond = function(index) {
   $scope.selectedRowSecond = index;

 var str = $scope.query;
 var strJoin = $scope.query;
 var strwhere = $scope.query;
  var compare1 = $scope.resultData4[$scope.selectedRow];
  var compare2 = $scope.resultData5[$scope.selectedRowSecond];
  var arrayPk = [];
  var arrayFk = [];
  //pego o primeiro elemento e coloco no array pk
  angular.forEach(compare1, function(element) {
    arrayPk.push(element);
  });
  //pego o primeiro elemento e coloco no array fk
  angular.forEach(compare2, function(element) {
    arrayFk.push(element);
  });
  var pk = arrayPk[0];
  var fk = arrayFk[0];



    //faço a comparacao
    if(pk == fk)
    {

    //verifico se foi adicionado uma junção com where e *
   if(!str.match(/ON/) && str.match(/,/) && str.match(/WHERE/)){

     $http.get('tabela/listaJsonResultadoTabelaEsquerdaJuncaoWhere/' + $scope.query  + '/' + pk).success(function (result7) {
     $http.get('tabela/listaJsonResultadoTabelaDireitaJuncaoWhere/' + $scope.query + '/' + pk).success(function (result8) {

         $scope.resultData7 = result7;
         $scope.resultData8 = result8;

         var str = $scope.query;
         str = str.split(".")[1].trim();
         strJoin = str.split("=")[0].trim();
         $scope.resultJoin = strJoin;
         //console.log(strJoin);
     });
   });

    //verifico se contem a string ON do INNER JOIN - sql individual join
   }else if(str.match(/ON/) && str.match(/,/) && !str.match(/WHERE/)){

      //sql individual tabela esquerda
      $http.get('tabela/listaJsonResultadoSimulacaojoinIndividualEsquerda/' + $scope.query  + '/' + $scope.tamanhoTabelaPrimaria  + '/' + $scope.tamanhoTabelaSecundaria +'/'+ $scope.tabelaPrimariaJoin +'/'+ $scope.tabelaSecundariaJoin + '/' + pk).success(function (resultJoinIndividualEsquerda) {

      $http.get('tabela/listaJsonResultadoSimulacaojoinIndividualDireita/' + $scope.query  + '/' + $scope.tamanhoTabelaPrimaria  + '/' + $scope.tamanhoTabelaSecundaria +'/'+ $scope.tabelaPrimariaJoin +'/'+ $scope.tabelaSecundariaJoin + '/' + pk).success(function (resultJoinIndividualDireita) {
      $scope.resultData7 = resultJoinIndividualEsquerda;
      $scope.resultData8 = resultJoinIndividualDireita;

      str = str.split("ON")[1].trim();
      strJoin = str.split("=")[0].trim();
      strJoin = strJoin.split(".")[1].trim();
      $scope.resultJoin = strJoin;

      });
    });
    // se nao adicionou o where no join
  }else if(str.match(/JOIN/) && !str.match(/WHERE/) && str.match(/ON/)){

        $http.get('tabela/listaJsonResultadoTabelaEsquerda/' + $scope.query  + '/' + pk).success(function (result7) {
        $http.get('tabela/listaJsonResultadoTabelaDireita/' + $scope.query + '/' + pk).success(function (result8) {
        //  console.log(result7);
            $scope.resultData7 = result7;
            $scope.resultData8 = result8;

            str = str.split("ON")[1].trim();
            strJoin = str.split("=")[0].trim();
            strJoin = strJoin.split(".")[1].trim();
            $scope.resultJoin = strJoin; //mostra resultado

        });
        });
      }else if(str.match(/,/) && str.match(/WHERE/)){

        //se foi adicionado o where na query
        str = str.split("WHERE")[1].trim();
        //console.log(str);
        strwhere = str.split("=")[0].trim();
        strwhere = strwhere.split(".")[1].trim();
        $scope.resultWhere = strwhere;


        $http.get('tabela/listaJsonResultadoSimulacaojoinIndividualEsquerdaWhere/' + $scope.query +'/'+ $scope.tabelaPrimariaJoin +'/'+ $scope.tabelaSecundariaJoin + '/' + pk + '/'+ str).success(function (resultJoinIndividualEsquerda) {

        $http.get('tabela/listaJsonResultadoSimulacaojoinIndividualDireitaWhere/' + $scope.query  +'/'+ $scope.tabelaPrimariaJoin +'/'+ $scope.tabelaSecundariaJoin + '/' + pk + '/'+ str).success(function (resultJoinIndividualDireita) {
        $scope.resultData7 = resultJoinIndividualEsquerda;
        $scope.resultData8 = resultJoinIndividualDireita;

        if(str.match(/ON/)){
            str = str.split("ON")[1].trim();
            strJoin = str.split("=")[0].trim();
            strJoin = strJoin.split(".")[1].trim();
            console.log(strJoin);
            $scope.resultJoin = strJoin; //mostra resultado
        }

        });
      });

    }else{

          //se foi adicionado o where na query
          str = str.split("WHERE")[1].trim();
          //console.log(str);
          strwhere = str.split("=")[0].trim();
          strwhere = strwhere.split(".")[1].trim();
          $scope.resultWhere = strwhere;


          $http.get('tabela/listaJsonResultadoTabelaEsquerdaWhere/' + $scope.query  + '/' + pk + '/'+ str).success(function (result7) {
          $http.get('tabela/listaJsonResultadoTabelaDireitaWhere/' + $scope.query + '/' + pk + '/'+ str).success(function (result8) {

              $scope.resultData7 = result7;
              if($scope.resultData7 != ""){
               $scope.resultData8 = result8; //mostra resultado
             }else{
               $scope.resultData8 = "";
             }
          });
         });
      }
      //metodo para mostrar o alert de sucesso da juncao
      $(function() {
          setTimeout(function() {
              $.bootstrapGrowl("Junção correta V", {
                  type: 'success',
                  align: 'center',
                  width: 'auto',
                  delay: 800,
                  allow_dismiss: true
              });
          }, 200);
      });

      //});
    }else{
        //metodo para mostrar o alert de erro da juncao
      $(function() {
          setTimeout(function() {
              $.bootstrapGrowl("Junção incorreta X", {
                  type: 'danger',
                  align: 'center',
                  width: 'auto',
                  delay: 800,
                  allow_dismiss: true
              });
          }, 200);
      });
      $scope.resultData6 = "";
      $scope.resultData7 = "";
      $scope.resultData8 = "";
    }

 }

//metodo para colorir e destacar os atributos
 $scope.color = function(row,index) {
     var whereResult =   $scope.resultWhere;
     var joinResult = $scope.resultJoin;
     var color = "#7ee5a2";
     if(whereResult == row)
     {
      return color;
     }
     if(joinResult == row)
     {
      return color;
     }
 }



  $scope.overCallback = function(event, ui) {

  };

  $scope.outCallback = function(event, ui) {

  };

  //metodo para remover os componente
  $scope.removeElement = function($index){
      $scope.list1.splice($index,1);
      arraySimbolo.splice($index,1);
      $scope.query = $scope.getQryAsString($scope.list1);
  };

    //metodo utilizado para ir concatenando as string da instrucao SQL
    $scope.getQryAsString = function(){
      var strResult = '';
      var fromPos = function(arr){
        var idx = 0;
        angular.forEach(arr,function(d,i){
          if (d.Field=="FROM")
            idx = 0;
        });
        return idx;
      }

      angular.forEach($scope.list1,function(d,i){
        strResult += d.Field + ' ';

        if (i>0
          && $scope.list1[0]
          && $scope.list1[0].Field == "SELECT"
          && d.Field != '*'
          && fromPos($scope.list1) > i+1){
            //strResult += ',';
            if (d.Field == '.'
              || $scope.list1[i-1].Field == '.'){
                strResult = strResult.slice(0,-2);
            }
        }else if(i>0){
          if (d.Field == '.'){
              strResult = strResult.slice(0,-3)+d.Field;
          }
        }
      });
    //  console.log(strResult);
      return strResult;
    }
//escondo e mostro os botoes
 $("#buttons").hide();
 $("#btnSimulacao").hide();
 $("#goback").hide();
 $("#advance").hide();
 $("#immediate").hide();
 $("#tabela1").hide();
 $("#tabela2").hide();
 $("#btnLogSQL").show();
$("#tabelasimula").hide();

});
