<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/simulacao/', function () {
    return view('simulacao/index');
});


Route::get('/simulacaoteste/', function () {
    return view('simulacao/teste');
});

Route::get('/produtos', 'ProdutoController@listaJson');

Route::get('/simulacao/metodo', 'TabelaController@metodo');


Route::get(
'/produtos/mostra/{id}',
'ProdutoController@mostra'
)
->where('id', '[0-9]+');


Route::get(
'/tabela/listaJsonResultado/{query}',
'TabelaController@listaJsonResultado');


Route::get(
'/tabela/listaJsonResultadoSimulacao/{query}/{pk}',
'TabelaController@listaJsonResultadoSimulacao');

Route::get(
'/tabela/listaJsonResultadoSimulacaojoinIndividualEsquerda/{query}/{tabelacount1}/{tabelacount2}/{tabela1}/{tabela2}',
'TabelaController@listaJsonResultadoSimulacaojoinIndividualEsquerda');

Route::get(
'/tabela/listaJsonResultadoSimulacaojoinIndividualDireita/{query}/{tabelacount1}/{tabelacount2}/{tabela1}/{tabela2}',
'TabelaController@listaJsonResultadoSimulacaojoinIndividualDireita');


Route::get(
'/tabela/listaJsonResultadoSimulacaojoinIndividualEsquerda/{query}/{tabelacount1}/{tabelacount2}/{tabela1}/{tabela2}/{pk}',
'TabelaController@listaJsonResultadoSimulacaojoinIndividualEsquerda');

Route::get(
'/tabela/listaJsonResultadoSimulacaojoinIndividualDireita/{query}/{tabelacount1}/{tabelacount2}/{tabela1}/{tabela2}/{pk}',
'TabelaController@listaJsonResultadoSimulacaojoinIndividualDireita');


Route::get(
'/tabela/listaJsonResultadoSimulacao/{query}',
'TabelaController@listaJsonResultadoSimulacao');


Route::get(
'/tabela/listaJsonResultadoTabelaEsquerda/{query}',
'TabelaController@listaJsonResultadoTabelaEsquerda');

Route::get(
'/tabela/listaJsonResultadoTabelaDireita/{query}',
'TabelaController@listaJsonResultadoTabelaDireita');

Route::get(
'/tabela/listaJsonResultadoTabelaEsquerda/{query}/{pk}',
'TabelaController@listaJsonResultadoTabelaEsquerda');

Route::get(
'/tabela/listaJsonResultadoTabelaDireita/{query}/{pk}',
'TabelaController@listaJsonResultadoTabelaDireita');

Route::get(
'/tabela/listaJsonResultadoTabelaEsquerdaWhere/{query}/{pk}/{where}',
'TabelaController@listaJsonResultadoTabelaEsquerdaWhere');

Route::get(
'/tabela/listaJsonResultadoTabelaDireitaWhere/{query}/{pk}/{where}',
'TabelaController@listaJsonResultadoTabelaDireitaWhere');

Route::get(
'/tabela/listaJsonResultadoSimulacaojoinIndividualEsquerdaWhere/{query}/{tabela1}/{tabela2}/{pk}/{where}',
'TabelaController@listaJsonResultadoSimulacaojoinIndividualEsquerdaWhere');


Route::get(
'/tabela/listaJsonResultadoSimulacaojoinIndividualDireitaWhere/{query}/{tabela1}/{tabela2}/{pk}/{where}',
'TabelaController@listaJsonResultadoSimulacaojoinIndividualDireitaWhere');

//juncao com where
Route::get(
'/tabela/listaJsonResultadoTabelaEsquerdaJuncaoWhere/{query}/{pk}',
'TabelaController@listaJsonResultadoTabelaEsquerdaJuncaoWhere');

//juncao com where
Route::get(
'/tabela/listaJsonResultadoTabelaDireitaJuncaoWhere/{query}/{pk}',
'TabelaController@listaJsonResultadoTabelaDireitaJuncaoWhere');



Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/estrutura', 'EstruturaController@index');
Route::get('/modulo1', 'ModuloController@modulo1');
Route::get('/simulacao', 'SimulacaoController@index');
Route::get('/questionario', 'QuestionarioController@index');
Route::get('/ajuda', 'AjudaController@index');
Route::get('/credito', 'CreditoController@index');
Route::get('/modulo1', 'ModuloController@modulo1');
Route::get('/modulo2', 'ModuloController@modulo2');
Route::get('/modulo3', 'ModuloController@modulo3');
Route::get('/modulos', 'ModuloController@index');
Route::get('/instrucoes', 'InstrucoesController@index');
Route::get('/preteste', 'PreTesteController@index');
Route::get('/pretesteopcoes', 'PreTesteController@opcoes');
Route::post('preteste/store', 'PreTesteController@store');
Route::post('/questionario/enviar','QuestionarioController@enviar');
Route::get('/tabela/listaJsonAluno', 'TabelaController@listaJsonAluno');
Route::get('/tabela/listaJsonDisciplina', 'TabelaController@listaJsonDisciplina');
Route::get('/tabela/listaJsonTurma', 'TabelaController@listaJsonTurma');
Route::get('/tabela', 'TabelaController@index');
Route::get('/tabela/listaJsonHistoricoEscolar', 'TabelaController@listaJsonHistoricoEscolar');

Route::get('/tabela/listaJsonPreRequisito', 'TabelaController@listaJsonPreRequisito');
