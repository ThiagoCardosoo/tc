@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')

<div class="container">
            <div class="titleOA">
              @if (Auth::user()->preteste == 0)
              <li class="dropdown" style="list-style-type:none; text-align: right; position: relative; right: 30px;">
                  <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>-->
                  <ul class="dropdown-menu" style="list-style-type:none" role="menu">
                      <li>
                          <a href="{{ route('logout') }}" title="Sair"
                              onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                              Sair
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                      </li>
                  </ul>
              </li>
                @endif
                <header class="codrops-header">
                <h1>Objeto de aprendizagem para o apoio ao ensino de SQL</h1>
                </header>
                <div class="container-middle">
                  <ul class="well" style="padding-left: 3em;">
                    <p>
                    Este Objeto de Aprendizagem (OA) possui o objetivo de apoiar o ensino e aprendizado da Linguagem SQL.  Estão disponíveis recursos de texto, imagens, simulações, além de atividades relacionadas às instruções para manipulação de dados e consultas sobre dados. Todas as atividades são executadas tendo como base um modelo de uma base cuja estrutura pode ser
                     visualizada no botão Como usar os recursos (abaixo).
                    </p>
                        <p>Conteúdo</p>
                        <li>Módulo com conceitos de Instruções SQL</li>
                        <li>Módulo com conceitos de Álgebra relacional</li>
                        <li>Módulo com conceitos de Seleção,Junção e Projeção</li>
                        <li>Blocos de comandos</li>
                        <li>Simulação de <strong class="text-danger">comandos</strong> executados.</li>
                      <p>
                        <p>
                         Para ter acesso aos recursos citados, é necessário realizar o pré-teste clicando no botão abaixo.
                      </p>

                      <a href="{{action('InstrucoesController@index')}}"><button type="button" class="btn btn-primary" role="button">Como usar os recursos</button>
                        @if (Auth::user()->preteste == 0)
                          <a href="{{action('PreTesteController@index')}}"><button type="button" class="btn btn-primary" role="button">Pré-Teste</button>
                        @else
                            <a href="{{action('PreTesteController@index')}}"><button type="button" class="btn btn-primary" role="button">Pré-Teste</button>
                           <a href="{{action('ModuloController@index')}}"><button type="button" class="btn btn-primary" role="button">Módulos</button>
                        @endif
                      </p>
                    </ul>
                </div>
            </div>

</div>
@endsection
