<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="Responsive Animated Border Menus with CSS Transitions" />
    <meta name="keywords" content="navigation, menu, responsive, border, overlay, css transition" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/icons.css" />
    <link rel="stylesheet" type="text/css" href="css/style2.css" />
    <link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css" />
    <script src="js/modernizr.custom.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/zui.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Objeto de aprendizagem para o apoio ao ensino de SQL</title>

    <!-- Styles -->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body id="bodyinstrucoes">

<div class="container">
            <div class="titleOAinstrucoes">
                <header class="codrops-header">
                  <h3 class="titleinstrucoes">Como usar os recursos</h3>
                </header>
                <div class="container-middle" style="padding-left: 1em;">
                    <p>
                      Este OA pode ser usado na sala de aula para montar instruções de uma consulta SQL. Por ser uma ferramenta interativa é possível mostrar a execução de consultas, resultados assim como também a simulação passo a passo ou instantânea do resultado de uma junção SQL.
                      Além disso a ferramenta também pode ser usada por alunos, como uma forma de estudo e revisão. Em particular a possibilidade de avançar e voltar os passos da simulação é bastante útil para destacar as mudanças entre cada passo feito em uma Junção SQL.
                    </p>
                    <p>
                      Inicialmente é aplicado um teste (Pré-Teste), após se completar ele é dado um feedback do seu resultado e após é liberado os modulos para estudo e simulação.
                    </p>
                    <p>
                      A seguir estão explicados o que são cada um dos elementos visuais do OA e de sua simulação:
                    </p>
                </div>
                <div class="btnVoltarinstrucoes">
                  <a  href="javascript:window.history.go(-1)"><button type="button" class="btn btn-success">Voltar</button></a>
                </div>
            </div>

            <div class="container">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Componentes</a></li>
                <li><a data-toggle="tab"  href="#menu1">Estrutura</a></li>
                <li><a data-toggle="tab"  href="#menu4">Módulos</a></li>
                <li><a data-toggle="tab" href="#menu2">Resultado da Consulta</a></li>
                <li><a data-toggle="tab"  href="#menu3">Simulação</a></li>
              </ul>

              <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                      <div class"container">
                        <div class="row">
                      <h3 style="margin-left:20px;">Componentes</h3>
                      <div class="col-md-3"><b>Botões</b><br />
                          <img src="{{ asset('imagens/botoes.png') }}" width="173" height="51"  alt="Botões" class="img-responsive" />
                          <p>
                            São usados para mudar o estado da simulação.
                              As funções são respectivamente: voltar ao estado anterior (se existir),
                              (voltar para o estado inicial),avançar para o próximo estado e executar a animação da simulação de modo automatico.
                          </p>
                      </div>
                      <div class="col-md-3"><b>Comandos</b>
                          <img src="{{ asset('imagens/comandos-sql.png') }}" width="263" height="352"  alt="Comandos" class="img-responsive" />
                          <p>
                            Blocos de comandos como SELECT,*,FROM etc são utilizados para montar a consulta SQL, arrasta-se os blocos
                            para a área do meio.<br />
                            Ajuda: Instruções de como usar a simulação.
                          </p>
                      </div>
                      <div class="col-md-3"><b>Tabelas/campos</b>
                          <img src="{{ asset('imagens/tabelas-campos.png') }}" width="262" height="469"  alt="Tabelas/Campos" class="img-responsive" />
                          <p>
                            O OA é baseado no Caso de Uso de uma estrutura pré-carregada
                            aqui estão localizados as tabelas e os campos necessarios para o encaixe das tabelas
                            junto aos comandos executados, assim montando a consulta desejada.
                            arrasta-se as tabelas e os campos para a área do meio.
                          </p>
                      </div>
                        <div class="col-md-3"><b>Ajuda</b>
                            <img src="{{ asset('imagens/erros.png') }}" width="308" height="158"  alt="Ajuda" class="img-responsive" />
                            <p>
                              Auxilia na montagem dos comandos, indicando os erros e dando algumas sugestões.
                            </p>
                        </div>
                        </div>
                    </div>

                    <div class"container" style="padding-top:20px;">
                      <div class="row">
                          <div class="col-md-5">
                            <b>Exercicio</b>
                                <img src="{{ asset('imagens/exercicio.png') }}" width="363" height="96"  alt="Exercicio" class="img-responsive" />
                                <p>
                                Exercício para se montar uma consulta.
                                </p>
                              </div>
                          <div class="col-md-6"><b>Instrução de bloco de comandos SQL</b>
                              <img src="{{ asset('imagens/instrucao-sql.png') }}" width="546" height="46"  alt="Ajuda" class="img-responsive" />
                              <p>
                              Este é um bloco de comando de instruções SQL montado.
                              </p>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div id="menu1" class="tab-pane fade">
                    <h3>Estrutura Caso de Uso</h3>
                    <div class"container">
                      <div class="row">
                        <img src="{{ asset('imagens/estrutura.png') }}" width="579" height="504"  alt="Ajuda" class="img-responsive" />
                        <p>
                        Esta é a estrutura das Tabelas que compoem o OA, baseado no caso de uso de uma universidade.
                        </p>
                        <p>
                        As ligações representam o relacionamento que existem entre as Chaves primária e estrangeira das tabelas.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div id="menu2" class="tab-pane fade">
                    <h3>Resultado da consulta</h3>
                    <div class"container">
                      <div class="row">
                        <img src="{{ asset('imagens/comando.png') }}" width="697" height="151"  alt="Comando de bloco sql montado." class="img-responsive" />
                        <p>
                          Comando de bloco sql montado.
                        </p>
                      </div>
                      <div class="row">
                            <img src="{{ asset('imagens/resultado.png') }}" width="707" height="647"  alt="Resultado da Consulta" class="img-responsive" />
                            <p>
                                Resultado da Junção de uma consulta SQL entre a Tabela Aluno e a Tabela HistoricoEscolar<br />
                                Mostrando a Tabela Aluno, Tabela HistoricoEscolar e a tabela com a junção dos dados com o Resultado do Inner Join.
                            </p>
                      </div>
                    </div>
                  </div>
                  <div id="menu3" class="tab-pane fade">
                    <h3>Simulação - Junção</h3>
                    <div class"container">
                      <div class="row">
                        <img src="{{ asset('imagens/comando.png') }}" width="697" height="151"  alt="Comando de bloco sql montado." class="img-responsive" />
                        <p>
                          Comando de bloco sql montado.
                        </p>
                      </div>
                      <div class="row">
                        <img src="{{ asset('imagens/simulacao.png') }}" width="699" height="566"  alt="Simulação" class="img-responsive" />
                        <p>
                          Resultado da simulação da junção feita entre as duas tabelas Aluno e Historico Escolar, Após
                          pressionado o botão de simulação passo a passo é mostrado o resultado do INNER JOIN feito
                          entre a linha da tabela Aluno com a linha da tabela HistoricoEscolar destacando a chave primária(CodAluno) e a chave estrangeira(CodAluno).
                        </p>
                      </div>
                    </div>
                  </div>
                  <div id="menu4" class="tab-pane fade">
                    <h3>Módulos</h3>
                    <div class"container">
                      <div class="row">
                        <img src="{{ asset('imagens/modulos.png') }}" width="579" height="504"  alt="Ajuda" class="img-responsive" />
                        <p>
                          O OA é composto de 3 Módulos com conteúdo pedagógico sobre a disciplina de <br/> Organização de banco de dados SQL.
                          <br />- <b>Módulo 1</b> - Comandos Iniciais da Linguagem SQL<br />
                          - <b>Módulo 2</b> - Álgebra relacional<br />
                          - <b>Módulo 3</b> - Relacionamento e Junções<br />
                          <br /> A simulação é baseada no Primeiro Módulo.
                        </p>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
        </div>



        </div>
</div>

<style type="text/css">

a {
  color: black !important;
 }

</style>

</html>
