@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')

 <link href="css/zui.css" rel="stylesheet" type="text/css">

 <div class="container-preteste">
             <div class="titleOA">
                 <header class="codrops-header">

                 <h1>Ajuda - Video Explicativo</h1><br />
                 </header>
                 <div class="container-middle">
                   <ul style="width: 60%; margin: 0 auto;">

                     <iframe width="420" height="315" src="https://www.youtube.com/embed/zRjnPfYN3mQ" frameborder="0" allowfullscreen></iframe>
                   </ul>
                 </div>
             </div>
        </div>
 </div>

 <div class="btnVoltar-ajuda">
   <a  href="home"><button type="button" class="btn btn-success">Voltar</button></a>
 </div>

 <style>
 .btn-success {
     color: #fff;
     text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
     background-color: #5bb75b;
     background-image: -moz-linear-gradient(top,#62c462,#51a351);
     background-image: -webkit-gradient(linear,0 0,0 100%,from(#62c462),to(#51a351));
     background-image: -webkit-linear-gradient(top,#62c462,#51a351);
     background-image: -o-linear-gradient(top,#62c462,#51a351);
     background-image: linear-gradient(to bottom,#62c462,#51a351);
     background-repeat: repeat-x;
     border-color: #51a351 #51a351 #387038;
     border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
     filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462',endColorstr='#ff51a351',GradientType=0);
     filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
 }
 </style>

@endsection
