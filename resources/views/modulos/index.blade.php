@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')

<div class="container">
        <header class="codrops-header">
          <li class="dropdown" style="list-style-type:none">
              <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  {{ Auth::user()->name }} <span class="caret"></span>
              </a>-->
              <ul class="dropdown-menu" style="list-style-type:none" role="menu">
                  <li>
                      <a href="{{ route('logout') }}" title="Sair"
                          onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                          Sair
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </li>
              </ul>
          </li>
          <br />
            <h1>Módulos<span>
            <nav class="codrops-demos">
                <a href="{{action('ModuloController@modulo1')}}" title="Modulo 1" class="current-demo">Módulo 1</a>
                <a href="{{action('ModuloController@modulo2')}}" title="Modulo 2">Módulo 2</a>
                <a href="{{action('ModuloController@modulo3')}}" title="Modulo 3">Módulo 3 </a>
                <!--<a href="#">Modulo 4</a>-->
            </nav>

            <p>Para acessar os recursos de simulação, visualizar a estrutura da base,<br/> acesse o menu ao lado.</p>
            <!--<div class="codrops-related">
                If you enjoyed this menu, you might also like:<br />
                <a href="http://tympanus.net/Development/MultiLevelPushMenu/">Multi-Level Push Menu</a><br/>
                <a href="http://tympanus.net/Development/SidebarTransitions/">Transitions for Off-canvas Navigations</a>
            </div>-->
        </header>

</div>
@endsection
