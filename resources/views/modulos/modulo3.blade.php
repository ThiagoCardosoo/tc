@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')


 <!-- css -->
<link href="css/zui.css" rel="stylesheet" type="text/css">
<style>

.papers-demo {
position:relative;
width:360px;
height:480px;
margin-left:auto;
margin-right:auto;
-webkit-transition: 0.5s;
-moz-transition: 0.5s;
-ms-transition: 0.5s;
-o-transition: 0.5s;
transition: 0.5s;
}

/* paper slider css*/
.paper-slide {
background:#eee;
box-shadow:0 0 15px rgba(0,0,0,.3);
}
.ps-nav {
display:block;
width:2em;
height:2em;
border:1px solid #aaa;
background:#ddd;
line-height:2em;
text-align:center;
position:absolute;
top:50%;
margin-top:-1em;
border-radius:2em;
}

#papers1{
width: 70%;
}

.ps-nav-prev {
left:-2.5em;
}
.ps-nav-next {
right:-2.5em;
}
.ps-nav:hover {
color:#eee;
background:#08c;
}
</style>


<div class="container">

        <header class="codrops-header">
            <h3>Módulo 3 - Relacionamento e Junções</h3>
        </header>

<!-- site content -->
<div id="wrapper">

<!-- paper slider units wrapper-->
<div class="mgtb papers-demo hide" id="papers1">

  <!-- one paper slider unit -->
  <div>
    <h1 class="aligncenter">Bem Vindo Ao Módulo 3</h1>
    <p class="aligncenter">Neste módulo você aprendera sobre Relacionamento de Tabelas e Junções<br />
        <img src="{{ asset('imagens/relacionamento.png') }}" width="573" height="317"  alt="Relacionamento de Tabelas e Junções" class="img-responsive" /><br />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Sumário - Página 2</h2>
    <ul class="list">
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="0">Inicio</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="2">Introdução</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="3">Relacionamento de Tabelas</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="4">Tipos de Junção</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="5">Tipos de Junção Conceitos</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="6">Junção Externas (OUTER JOIN)</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="7">Opções de junção</a></li>
    </ul>
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 3 - Introdução</h2>
    <div class="contentModule">
          <div class="container-middle">
            <p>
              <div class="divrolagem">
                SQL é uma linguagem estruturada de consulta desenvolvida pela IBM na decada de 70, se tornou
                a linguagem padrão dos banco de dados atualmente.Os banco de dados relacionais armazenam seus dados em tabelas composta de linhas e colunas,
              onde cada coluna corresponde a um campo e cada linha a um registro.
              Hoje para efetuamos consultas nos principais banco de dados como Mysql,SQlServer,Oracle, Firebird, Db2 é preciso montar instruções SQL.
              Uma instrução SQL é composta de comandos que são denominados Cláusulas SQL.
            </div>
          </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p><br />
  </div>
  <!-- /one paper slider unit -->

  <div>
    <h2 class="aligncenter">Página 4 - Relacionamento de Tabelas </h2>
      <div class="contentModule">
          <div class="container-middle">
              <p>
                <div class="divrolagem">
                  Para relacionar uma tabela com outra, precisamos apenas que a chave primária de uma esteja presente como chave secundaria na outra.
                  <img src="{{ asset('imagens/relacionamento-casodeuso.png') }}" class="imgModule" width="356" height="157"  alt="Relacionamento de Tabelas" class="img-responsive" />
                    <br />A imagem mostra o relacionamento entre a Tabela Aluno e a Tabela HistoricoEscolar através da chave primária CodAluno e a Chave secundaria CodAluno
                    da Tabela HistoricoEscolar.
               </div>
              </p>
          </div>
      </div>
      <p class="aligncenter">
        <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
        <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
      </p><br />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 5 - Tipos de Junção </h2>
      <div class="contentModule">
          <div class="container-middle">
              <p>
                <div class="divrolagem">
                  Tipos de junção:  <br />
                <br />INNER: Retorna o número de linhas que satisfazem o predicado de junção.
                <br />Condição pode ser qualquer comparação, de igualdade, >, < ou outra.
                <br />A junção pode ser feita em duas operações (FROM + WHERE)
                ou somente na cláusula FROM (tab1 INNER JOIN tab2 ON condição.
               </div>
              </p>
          </div>
      </div>
      <p class="aligncenter">
        <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
        <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
      </p><br />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 6 - Tipos de Junção Conceitos</h2>
    <div class="contentModule">
        <div class="container-middle">
            <p>
              <div class="divrolagem">
                Tipos de junção (conceitos):
              <br />EQUI-JOIN: Junção que admite somente condição de igualdade. Muitos SGBDs não possuem operador diferente da junção INNER.
              <br />NATURAL JOIN: Conceito que define uma junção sem definir condição, neste caso a junção é feito com os atributos com os mesmos nomes e a condição é de igualdade.
              <br />CROSS-JOIN: Apresenta o mesmo resultado do produto cartesiano.
             </div>
            </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

  <div>
    <h2 class="aligncenter">Página 7 - Junção Externas (OUTER JOIN)</h2>
    <div class="contentModule">
        <div class="container-middle">
            <p>
              <div class="divrolagem">
                <br />LEFT: Mostra todos os registros da primeira tabela, mesmo que não estejam associados a nenhum da segunda.
                <br />RIGHT: Todos os registros da segunda tabela são exibidos, mesmo não estando associados a nenhum da primeira.
                <br />FULL: Combina os resultados do dois outros JOINs externos, mostra no mínimo uma vez os registros de cada tabela, mesmo que não exista nenhum associado na outra.
             </div>
            </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>
  <!-- /one paper slider unit -->

  <div>
    <h2 class="aligncenter">Página 8 - Opções de junção</h2>
    <div class="contentModule">
        <div class="container-middle">
            <p>
              <div class="divrolagem">
                <br />Junções externas necessitam o uso da opção na cláusula FROM com ON, mas as demais podem ser feitas usando FROM e WHERE.
                <br />Se houverem várias junções, deve haver cuidado para efetuar uma junção e o resultado dela com outro resultado ou outra tabela.
                <br />É fundamental conhecer princípios da álgebra.
                <br />Exemplo junção externa à esquerda: Mostrando todas as cidades, mesmo as que não possuem funcionários associados.
                <br />SELECT *
                <br />FROM cidade c left join funcionario f ON
                <br />f.codcid = c.codcid
                <br />A junção pode ser RIGHT, onde todos os registros da segunda tabela são exibidos.
             </div>
            </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

</div>
<!-- /paper slider units wrapper -->
</div>

 @include('layouts.btnvoltar')


<script src="js/jquery.paper-slider.js"></script>
<script>
//scripts
$(document).ready(function() {


//init slider 1
  var defaults = {
    speed: 500
    ,timer: 4000
    ,autoSlider: false
    ,hasNav: true
    ,pauseOnHover: true
    ,navLeftTxt: '&lt;'
    ,navRightTxt: '&gt;'
    ,zIndex:20
    ,ease: 'linear'
    ,beforeAction: function() {
      //this refers to DS instance
      this.t.css({
        background: '#08c'
      })
    }
    ,afterAction: function() {
      this.t.css({
        background: '#eee'
      })
      //this refers to DS instance
    }
  }
,as = $('#papers1').paperSlider(defaults)
,count = 2


})

</script>

</div>
@endsection
