@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')


 <!-- css -->
<link href="css/zui.css" rel="stylesheet" type="text/css">
<style>

.papers-demo {
position:relative;
width:360px;
height:480px;
margin-left:auto;
margin-right:auto;
-webkit-transition: 0.5s;
-moz-transition: 0.5s;
-ms-transition: 0.5s;
-o-transition: 0.5s;
transition: 0.5s;
}

/* paper slider css*/
.paper-slide {
background:#eee;
box-shadow:0 0 15px rgba(0,0,0,.3);
}
.ps-nav {
display:block;
width:2em;
height:2em;
border:1px solid #aaa;
background:#ddd;
line-height:2em;
text-align:center;
position:absolute;
top:50%;
margin-top:-1em;
border-radius:2em;
}

#papers1{
width: 70%;
}

.ps-nav-prev {
left:-2.5em;
}
.ps-nav-next {
right:-2.5em;
}
.ps-nav:hover {
color:#eee;
background:#08c;
}
</style>


<div class="container">

        <header class="codrops-header">
            <h3>Módulo 2 - Álgebra relacional</h3>
        </header>

<!-- site content -->
<div id="wrapper">

<!-- paper slider units wrapper-->
<div class="mgtb papers-demo hide" id="papers1">

  <!-- one paper slider unit -->
  <div>
    <h1 class="aligncenter">Bem Vindo Ao Módulo 2</h1>
    <p class="aligncenter">Neste módulo você aprendera a Álgebra relacional<br />
    <img src="{{ asset('imagens/produto-cartesiano.png') }}" width="570" height="236"  alt="Produto Cartesiano Álgebra relacional" class="img-responsive" />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Sumário - Página 2</h2>
    <ul class="list">
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="0">Inicio</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="2">Introdução</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="3">A álgebra Relacional</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="4">Seleção</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="5">Projeção</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="6">Junção</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="7">Produto Cartesiano</a></li>
    </ul>
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 3 - Introdução</h2>
    <div class="contentModule">
          <div class="container-middle">
              <p>
              <div class="divrolagem">
                  A álgebra relacional é o conjunto base de operações para o modelo relacional utilizado para manipular as relações. As operações são utilizadas para selecionar
               tuplas de relações individuais e combinar tuplas relacionadas e relações diferentes para especificar uma consulta em um determinado banco de dados. O resultado de cada operação é uma nova operação,
               que também pode ser manipulada pela álgebra relacional.
             </div>
             </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p><br />
  </div>
  <!-- /one paper slider unit -->

  <div>
    <h2 class="aligncenter">Página 4 - A álgebra Relacional </h2>
      <div class="contentModule">
          <div class="container-middle">
              <p>
                <div class="divrolagem">
                  As operações da álgebra relacional são dividida em dois grupos.um grupo que constitui a teoria do conjunto da matemática aonde cada relação é definida como um conjunto de tupla no modelo relacional que incluem as operações de conjunto UNIÃO,INTERSECÇAO,DIFERENÇA DE CONJUNTO E PRODUTO CARTESIANO e o outro grupo no qual será descrito consiste em operação desenvolvidas para o banco de dados relacionais SELEÇÃO,PROJEÇÃO,E JUNÇÃO etc.
                  <img src="{{ asset('imagens/quadro-algebra.jpg') }}" class="imgModule" width="865" height="489"  alt="Composição de um banco de dados relacional" class="img-responsive" />
               </div>
              </p>
          </div>
      </div>
      <p class="aligncenter">
        <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
        <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
      </p><br />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 5 - Seleção </h2>
      <div class="contentModule">
          <div class="container-middle">
              <p>
                <div class="divrolagem">
                       Seleção de tuplas (linhas) que atendem uma certa característica ou condição.
                      A forma geral de uma operação select é:<br />
                      < condição de seleção > ( nome da relação )<br />
                      A letra grega é utilizada para representar a operação de seleção;
                      condição de seleção é uma expressão booleana aplicada sobre os atributos da relação
                      e nome da relação é o nome da relação sobre a qual será aplicada a operação select.<br />
                      Exemplo: Selecionar a tupla empregado cujo o salário é maior que 2.500,00
                      consulta  =   salário < 2.500,00 (EMPREGADO)<br />
                      Ex: resultado
                        <img src="{{ asset('imagens/resultado-selecao.png') }}" class="imgModule" width="630" height="122"  alt="Exemplo álgebra relacional - seleção" class="img-responsive" />
               </div>
              </p>
          </div>
      </div>
      <p class="aligncenter">
        <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
        <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
      </p><br />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 6 - Projeção</h2>
    <div class="contentModule">
        <div class="container-middle">
            <p>
              <div class="divrolagem">
                A operação projeção seleciona um conjunto determinado de colunas de uma relação. A forma geral de uma operação projeção é:
              lista de atributos (nome da relação)
              A letra grega representa a operação projeção lista de atributos representa a lista de atributos que o usuário deseja selecionar
               e nome da relacao representa a relação sobre a qual a operação projeção será aplicada.

              <br />Exemplo: Listar o último nome, primeiro nome e salário de cada empregado.

              consulta  =   nome,sobrenome,salario (EMPREGADO)

              <img src="{{ asset('imagens/resultado-projecao.png') }}" class="imgModule" width="630" height="122"  alt="Exemplo álgebra relacional - projeção" class="img-responsive" />
             </div>
            </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

  <div>
    <h2 class="aligncenter">Página 7 - Junção</h2>
      <div class="contentModule">
        <div class="container-middle">
                        <p>
                          <div class="divrolagem">
                            A junção retorna a combinação de tuplas de duas relações de duas tabelas por exemplo R1 e R2 que apresentam uma característica;
                            A forma geral da operação junção entre duas tabelas R e S é a seguinte:
                            R |X| condição de junção S
                          <br />  Exemplo: Encontre todos os empregados que desenvolvem projetos em santa catarina.
                            consulta = EMPREGADO/PROJETO |X| PROJETO numero_projeto = numero
                          <img src="{{ asset('imagens/resultado-juncao.png') }}" class="imgModule" width="630" height="122"  alt="Exemplo álgebra relacional - projeção" class="img-responsive" />
                         </div>
                        </p>
                  </div>
          </div>
          <p class="aligncenter">
            <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
            <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
          </p>
  </div>
  <!-- /one paper slider unit -->

  <div>
    <h2 class="aligncenter">Página 8 - Produto Cartesiano</h2>
    <div class="contentModule">
        <div class="container-middle">
            <p>
              <div class="divrolagem">
                O produto cartersiano retorna todas as combinações de tuplas de duas tabelas R1 e R2.
                O formato geral do produto cartesiano entre duas tabelas R1 e R2 é:
                     R1|X| R2
                <br />Exemplo: Encontre todos os empregados que desenvolvem projetos em campinas.
                consulta = EMPREGADO/PROJETO |X| PROJETO
              <img src="{{ asset('imagens/resultado-produto.png') }}" class="imgModule" width="630" height="122"  alt="Exemplo álgebra relacional - projeção" class="img-responsive" />
             </div>
            </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

</div>
<!-- /paper slider units wrapper -->
</div>

 @include('layouts.btnvoltar')


<script src="js/jquery.paper-slider.js"></script>
<script>
//scripts
$(document).ready(function() {


//init slider 1
  var defaults = {
    speed: 500
    ,timer: 4000
    ,autoSlider: false
    ,hasNav: true
    ,pauseOnHover: true
    ,navLeftTxt: '&lt;'
    ,navRightTxt: '&gt;'
    ,zIndex:20
    ,ease: 'linear'
    ,beforeAction: function() {
      //this refers to DS instance
      this.t.css({
        background: '#08c'
      })
    }
    ,afterAction: function() {
      this.t.css({
        background: '#eee'
      })
      //this refers to DS instance
    }
  }
,as = $('#papers1').paperSlider(defaults)
,count = 2


})

</script>

</div>
@endsection
