@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')


 <!-- css -->
<link href="css/zui.css" rel="stylesheet" type="text/css">
<style>


.papers-demo {
position:relative;
width:360px;
height:480px;
margin-left:auto;
margin-right:auto;
-webkit-transition: 0.5s;
-moz-transition: 0.5s;
-ms-transition: 0.5s;
-o-transition: 0.5s;
transition: 0.5s;
}

/* paper slider css*/
.paper-slide {
background:#eee;
box-shadow:0 0 15px rgba(0,0,0,.3);
}
.ps-nav {
display:block;
width:2em;
height:2em;
border:1px solid #aaa;
background:#ddd;
line-height:2em;
text-align:center;
position:absolute;
top:50%;
margin-top:-1em;
border-radius:2em;
}

#papers1{
width: 70%;
}

.ps-nav-prev {
left:-2.5em;
}
.ps-nav-next {
right:-2.5em;
}
.ps-nav:hover {
color:#eee;
background:#08c;
}
</style>


<div class="container">

        <header class="codrops-header">
            <h3>Módulo 1 - Comandos Iniciais da Linguagem SQL</h3>
        </header>

<!-- site content -->
<div id="wrapper">

<!-- paper slider units wrapper-->
<div class="mgtb papers-demo hide" id="papers1">

  <!-- one paper slider unit -->
  <div>
    <h1 class="aligncenter">Bem Vindo Ao Módulo 1</h1>
    <p class="aligncenter">Neste módulo você aprendera os conceitos iniciais da linguagem SQL<br /><br />
        <img src="{{ asset('imagens/tabelas5.png') }}" width="570" height="236"  alt="Junções SQL" class="img-responsive" />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Sumário - Página 2</h2>
    <ul class="list">
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="0">Inicio</a></li>
      <!--<li><a class="li-head ps-link" href="javascript:;" data-ps-page="1">Clausulas</a></li>-->
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="2">Introdução</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="3">Composição de um banco de dados relacional</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="4">Forma da instrução Select</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="5">Modelo de Execução</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="6">Produto Cartesiano</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="7">Seleções de Registros (Where)</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="8">Junção (join)</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="9">Elaboração de consultas</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="10">Eliminação de duplicatas</a></li>
      <li><a class="li-head ps-link" href="javascript:;" data-ps-page="11">Ordenação</a></li>
    </ul>
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 3 - Introdução</h2>
    <div class="contentModule">
          <div class="container-middle">
            <p>
              <div class="divrolagem">
             Uma instrução de consulta SQL permite a recuperação de dados de uma ou mais relações (tabelas, visões) existentes em uma base de dados.
              <br /> Não é necessário especificar a forma de recuperação e a ordem na qual os dados serão recuperados.
              <br /> SQL é uma linguagem não procedural;
              <br /> Instruções de consulta podem ser extremamente complexas.
              <br /> Para melhor entendimento inicia-se por instruções simples, chegando progressivamente às mais complexas.
            </div>
          </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p><br />
  </div>
  <!-- /one paper slider unit -->

  <div>
    <h2 class="aligncenter">Página 4 - Composição de um banco de dados relacional </h2>
      <div class="contentModule">
          <div class="container-middle">
              <p>
                <div class="divrolagem">
                  Um banco de dados relacional é composto por tabelas ou relações, uma tabela é composta por um conjunto de linhas(tuplas) cada linha possui uma série de campos(valor de atributo). Cada campo é identificado por nome de campo(nome de atributo). Um conjunto de campos de linhas de uma tabela que possui o mesmo nome formam uma coluna.
                  Ex:<br />
                  <img src="{{ asset('imagens/tabela.png') }}" class="imgModule" width="380" height="180"  alt="Composição de um banco de dados relacional" class="img-responsive" />
               </div>
              </p>
          </div>
      </div>
      <p class="aligncenter">
        <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
        <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
      </p><br />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 5 - Forma da instrução Select </h2>
      <div class="contentModule">
          <div class="container-middle">
              <p>
                <div class="divrolagem">
                  SELECT(operação de projeção)<br />
                  FROM(produto cartesiano)<br />
                  WHERE(seleção)<br />
                  O conhecimento da estrutura e operadores da álgebra relacional é importante no entendimento do processo de execução das consultas.<br />
                  A cláusula WHERE não é obrigatória, mas é usada com muita frequencia.
               </div>
              </p>
          </div>
      </div>
      <p class="aligncenter">
        <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
        <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
      </p><br />
  </div>
  <!-- /one paper slider unit -->

  <!-- one paper slider unit -->
  <div>
    <h2 class="aligncenter">Página 6 - Modelo de Execução</h2>
    <div class="contentModule">
          <div class="container-middle">
          <p>
            <div class="divrolagem">
              Modelo canônico de execução de uma consulta SQL:  <br />
              <br />  1.É feito um produto cartesiano de todas as tabelas, relações envolvidas, citadas na cláusula FROM.
              <br />  2.São selecionadas todas as linhas que obedecem aos critérios definidos na cláusula WHERE.
              <br /> 3.É feita a projeção das colunas que vão ao resultado, colunas definidas na cláusula SELECT.
               <br /><b>Exemplo de consultas SQL:</b>  <br />
                <br />SELECT CodDep, Descricao
                <br />FROM Departamento
                <br />Ou
                <br />  SELECT *
                <br />FROM Departamento
                <br />A cláusula FROM apresenta somente uma tabela, portanto não há produto cartesiano.
                <br />São projetados dois campos em ambos os casos, no segundo usa-se * para indicar que serão mostrados todos os campos.
           </div>
          </p>
        </div>
    </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

  <div>
    <h2 class="aligncenter">Página 7 - Produto Cartesiano</h2>
        <div class="container-middle">
        <p>
          <div class="divrolagem">
            Na matemática, produto cartesiano de dois conjuntos é o conjunto de todos os pares ordenados, tais que, em cada par, o primeiro elemento vem do primeiro conjunto e o segundo vem do segundo conjunto.
            <br />Em SQL, o produto cartesiano é um combinação de todos os registros de uma tabela com todos da outra, sem considerar a relação existente entre estas tabela.<br />
            O número final de linhas é o resultado da multiplicação do número de linhas da primeira tabela pelo número de linhas da segunda.
          <br />O número de colunas (caso não seja definido na cláusula SELECT ) é a soma das colunas das duas tabelas.
          <br />Exemplo:
          <br />SELECT *
          <br />FROM cidade, funcionario
         </div>
        </p>
      </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>
  <!-- /one paper slider unit -->

  <div>
    <h2 class="aligncenter">Página 8 - Seleções de Registro (Where)</h2>
    <div class="container-middle">
    <p>
      <div class="divrolagem">
        <br />Operadores para seleção:
        <br />• Comparações: >, <, >=, <=, =
        <br />• Negação: NOT ou !
        <br />• CodDep != 1
        <br />• LIKE: Conteúdo interno em um campo
        <br />• '%Rua%': Qualquer valor que
        <br />contenha a palavra Rua
        <br />• 'Rua%': Rua no início
        <br />• '_%'': Pelo menos um caractere
        <br />Operadores para seleção:
        <br />•No operador LIKE os caracteres % e _ significam um conjunto qualquer de caracteres e um caractere respectivamente.
        <br />•Operador LIKE possui alto custo de processamento.
        <br />•BETWEEN: Entre dois valores
        <br />•Ex.: valor BETWEEN 10 AND 15
        <br />•Conectores lógicos: AND, OR.
     </div>
    </p>
  </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

  <div>
    <h2 class="aligncenter">Página 9 - Junção (join)</h2>
    <div class="container-middle">
    <p>
      <div class="divrolagem">
        A operação de junção é usada para produzir a lista dos registros de duas tabelas que estão relacionados através de atributos comuns.
        <br />O caso mais comum de junção é a chamada junção natural, na qual existe igualdade de valores, porém as junções podem envolver outros critérios de comparação, não apenas igualdade.
        <br />O termo geral “junção” na maioria dos casos é usado para denotar uma junção natural.
      <br />Junção natural, usando cláusula WHERE:
      <br />WHERE cidade.codcid = funcionario.codcid.
      <br />É necessário colocar o nome da tabela em frente ao nome do campo porque ambas as tabelas possuem campos com o mesmo nome (codcid).
      <br />Na linguagem SQL diferentemente da álgebra relacional é necessário especificar os critérios de junção, mesmo que os nomes das colunas sejam os mesmos.
     </div>
    </p>
  </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

  <div>
    <h2 class="aligncenter">Página 10 - Elaboração de consultas</h2>
    <div class="container-middle">
    <p>
      <div class="divrolagem">
        <br /> Passos básicos para elaboração de uma consulta (SELECT, FROM, WHERE):
        <br /> 1.Definir quais as tabelas que deverão constar na cláusula FROM.
        <br /> 2.Efetuar a junção, selecionando apenas os registros relacionados.
        <br /> 3.Projetar os campos que devem aparecer no resultado.
        <br /> 4.Definir outros critérios de seleção.
        <br /> 5.Definir agrupamentos, operações sobre campos, ordenação, etc.
     </div>
    </p>
  </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

  <div>
    <h2 class="aligncenter">Página 11 - Eliminação de duplicatas</h2>
    <div class="container-middle">
    <p>
      <div class="divrolagem">
        <br />Exemplo de consulta com valores repetidos: Mostrar o código de todos os departamentos já usados em alguma movimentação de produto.
      <br />SELECT coddep
      <br />FROM movest
      <br />É necessária somente a tabela MovEst porque a mesma possui o código do departamento.
      No SQL mostrado, se um mesmo departamento tiver sido usado mais de uma movimentação, o código correspondente irá aparecer no resultado igual número de vezes.
      <br />Na álgebra relacional, valores não são mostrados mais de uma vez, mas em SQL este é o padrão;
      <br />Para não haver valores duplicados, é necessário usar uma cláusula DISTINCT.<br />
      SELECT DISTINCT coddep FROM movest
      A eliminação de valores duplicadas envolve toda a cláusula SELECT, isto significa que se forem projetados vários campos só haverá eliminação se ocorrer a repetição do resultado completo.
      <br />Ex. SELECT DISTINCT coddep, matfunc FROM movest.
      <br />Neste exemplo, se houverem resultados com valores iguais em ambos os campos, haverá eliminação, mas se apenas o código de departamento for o mesmo de outro registro a eliminação não ocorre.
     </div>
    </p>
  </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

  <div>
    <h2 class="aligncenter">Página 12 - Ordenação</h2>
    <div class="container-middle">
    <p>
      <div class="divrolagem">
        Ordenação de resultados: Qualquer combinação de colunas, mesmo os que não são projetados pela cláusula SELECT.<br />
        A cláusula ORDER BY permite indicar uma lista de campos que definem a ordem de exibição dos dados.
        <br />Ex.: ORDER BY descricao.
        <br />Quando houver mais de um campo, os mesmos serão separados por “,”.
        <br />ORDER é sempre a última cláusula.
     </div>
    </p>
  </div>
    <p class="aligncenter">
      <a class="btn btn-b btn-inline ps-link" href="javascript:;" data-ps-page="0">Voltar para a Página 1</a>
      <a class="btn btn-c btn-inline ps-link" href="javascript:;" data-ps-page="1">Voltar para a Página 2</a>
    </p>
  </div>

</div>
<!-- /paper slider units wrapper -->


</div>

 @include('layouts.btnvoltar')

<script src="js/jquery.paper-slider.js"></script>
<script>
//scripts
$(document).ready(function() {


//init slider 1
  var defaults = {
    speed: 500
    ,timer: 4000
    ,autoSlider: false
    ,hasNav: true
    ,pauseOnHover: true
    ,navLeftTxt: '&lt;'
    ,navRightTxt: '&gt;'
    ,zIndex:20
    ,ease: 'linear'
    ,beforeAction: function() {
      //this refers to DS instance
      this.t.css({
        background: '#08c'
      })
    }
    ,afterAction: function() {
      this.t.css({
        background: '#eee'
      })
      //this refers to DS instance
    }
  }
,as = $('#papers1').paperSlider(defaults)
,count = 2


})

</script>

</div>
@endsection
