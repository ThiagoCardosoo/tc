@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')

 <style>

 .demo {
   width: 100%;
   height: 360px;
 }
 </style>

  <link href="css/zui.css" rel="stylesheet" type="text/css">

      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="css/demo.css" />
      <link rel="stylesheet" type="text/css" href="css/icons.css" />
      <link rel="stylesheet" type="text/css" href="css/style2.css" />

<style>
.btn-success {
    color: #fff;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
    background-color: #5bb75b;
    background-image: -moz-linear-gradient(top,#62c462,#51a351);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#62c462),to(#51a351));
    background-image: -webkit-linear-gradient(top,#62c462,#51a351);
    background-image: -o-linear-gradient(top,#62c462,#51a351);
    background-image: linear-gradient(to bottom,#62c462,#51a351);
    background-repeat: repeat-x;
    border-color: #51a351 #51a351 #387038;
    border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462',endColorstr='#ff51a351',GradientType=0);
    filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
</style>

<script type="text/javascript">

  $(document).ready(function() {

    var data = {

      operators: {

       operator1: {
          top: 20,
          left: 20,
          properties: {
            title: 'Aluno',
            outputs: {
	      	<?php
	      		foreach ($alunos as $a)
				{
				  $Field = $a->Field;
				  $Type = $a->Type;
	      	?>
              <?php echo $Field; ?>: {

                label: '<?php echo $Field; ?>',
              },
		      	<?php
		        	}
		        ?>
            },
          }
        },

        operator2: {
          top: 20,
          left: 170,
          properties: {
            title: 'Disciplina',
            outputs: {
	      	<?php
	      		foreach ($disciplinas as $d)
				{
				  $Field = $d->Field;
				  $Type = $d->Type;
	      	?>
              <?php echo $Field; ?>: {

                label: '<?php echo $Field; ?>',
              },
		      	<?php
		        	}
		        ?>
            },
          }
        },

        operator3: {
          top: 20,
          left: 400,
          properties: {
            title: 'Turma',
            inputs: {
	      	<?php
	      		foreach ($turmas as $t)
				{
				  $Field = $t->Field;
				  $Type = $t->Type;

	      	?>
              <?php echo $Field; ?>: {

                label: '<?php echo $Field; ?>',
              },
		      	<?php
		        	}
		        ?>
            },
             outputs: {
              Pk: {
                label: 'Pk',
              }
            }
          }
        },

        operator4: {
          top: 200,
          left: 400,
          properties: {
            title: 'PreRequisito',
            inputs: {
          <?php
            foreach ($prerequisitos as $p)
        {
          $Field = $p->Field;
          $Type = $p->Type;
          ?>
              <?php echo $Field; ?>: {

                label: '<?php echo $Field; ?>',
              },
            <?php
              }
            ?>
            },
          }
        },
       operator5: {
          top: 200,
          left: 20,
          properties: {
            title: 'HistoricoEscolar',
            inputs: {
          <?php
            foreach ($historicos as $h)
        {
          $Field = $h->Field;
          $Type = $h->Type;
          ?>
              <?php echo $Field; ?>: {

                label: '<?php echo $Field; ?>',
              },
            <?php
              }
            ?>
            },
          }
        },
      },

      links: {
        link_1: {
          fromOperator: 'operator2',
          fromConnector: 'CodDisciplina',
          toOperator: 'operator3',
          toConnector: 'CodDisciplina',
        },
        link_2: {
          fromOperator: 'operator1',
          fromConnector: 'CodAluno',
          toOperator: 'operator5',
          toConnector: 'CodAluno',
        },
        link_3: {
          fromOperator: 'operator3',
          fromConnector: 'Pk',
          toOperator: 'operator5',
          toConnector: 'CodTurma',
        },
        link_4: {
          fromOperator: 'operator2',
          fromConnector: 'CodDisciplina',
          toOperator: 'operator3',
          toConnector: 'CodDisciplina',
        },

      }


    };

    // Apply the plugin on a standard, empty div...
    $('#example').flowchart({
      data: data
    });
  });

</script>



<div class="container">
  <div class="row">
    <div class="col-md-10">
      <div class="container-estrutura-case">
              <header class="codrops-header">
                  <h1>Estrutura - Caso de uso (Universidade)</h1>

                  <div class="container-estrutura">
      		             <div class="demo" id="example"></div>
      			     </div>
              </header>
      </div>
    </div>
  </div>
  <div class="row">
      <div class="col-md-1"></div>
    <div class="col-md-8">

        <ul class="well">
          <p>
            <b>Estrutura das tabelas (Relacionamentos)</b><br />
            <h5><b>Tabela Aluno/HistoricoEscolar</b></h5>
            A Tabela Aluno onde esta armazenado os dados do Aluno possui relacionamento
            com a tabela HistoricoEscolar Através da Chave Primária(PK) CodAluno da tabela Aluno e a Chave Estrangeira(FK) CodAluno da tabela HistoricoEscolar.

            <h5><b>Tabela Disciplina/Turma</b></h5>
            A Tabela Disciplina onde esta armazenado os dados da Disciplina possui relacionamento
            com a tabela Turma Através da Chave Primária(PK) CodDisciplina da tabela Disciplina e a Chave Estrangeira(FK) CodDisciplina da tabela Turma.

            <h5><b>Tabela Turma/HistoricoEscolar</b></h5>
            A Tabela Turma onde esta armazenado os dados da Turma possui relacionamento
            com a tabela HistoricoEscolar Através da Chave Primária(PK) CodTurma da tabela Turma e a Chave Estrangeira(FK) CodTurma da tabela HistoricoEscolar.

            <h5><b>Tabela Disciplina/PreRequisito</b></h5>
            A Tabela Disciplina onde esta armazenado os dados da Disciplina possui relacionamento
            com a tabela PreRequisito Através da Chave Primária(PK) CodDisciplina da Disciplina e a Chave Estrangeira(FK) CodTurma da tabela PreRequisito.
          </p>
          </p>
        </ul>

    </div>
  </div>
</div>

<div class="btnVoltar-estrutura">
  <a  href="modulos"><button type="button" class="btn btn-success">Voltar</button></a>
</div>

@endsection
