<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="Objeto de aprendizagem para o apoio ao ensino de SQL" />
    <meta name="keywords" content="SQL, Banco de dados, Ensino, Objeto de aprendizagem, Objeto de aprendizagem SQL" />
    <meta name="author" content="Thiago Cardoso" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/icons.css" />
    <link rel="stylesheet" type="text/css" href="css/style2.css" />
    <link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css" />
    <script src="js/modernizr.custom.js"></script>
    <!--<link href="/css/app.css" rel="stylesheet">-->

    <title>Objeto de aprendizagem para o apoio ao ensino de SQL</title>

    <!-- Styles -->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>

    <div id="app">

        <nav id="bt-menu" class="bt-menu">
                <a href="#" class="bt-menu-trigger"><span>Menu</span></a>
                <ul>
                    @if (Auth::user()->preteste == 0)
                      <li><a href="{{action('HomeController@index')}}" class="bt-icon icon-home" title="inicio">Inicio</a>Inicio</li>
                      <li><a href="{{action('AjudaController@index')}}" class="fa fa-question-circle-o fa-5x" title="ajuda">Ajuda</a>Ajuda</li>
                      <li><a href="{{action('CreditoController@index')}}" class="fa fa-mortar-board fa-5x" title="creditos">Creditos</a>Creditos</li>
                    @endif
                    @if (Auth::user()->preteste == 1)
                      <li><a href="{{action('HomeController@index')}}" class="bt-icon icon-home" title="inicio">Inicio</a>Inicio</li>
                      <li><a href="{{action('ModuloController@index')}}" class="fa fa-newspaper-o" title="módulos">Módulos</a>Módulos</li>
                      <li><a href="{{action('EstruturaController@index')}}" class="fa fa-table fa-5x" title="estrutura">Estrutura</a>Estrutura</li>
                      <li><a href="{{action('SimulacaoController@index')}}" class="bt-icon icon-refresh" title="simulacao">Simulação</a>Simulação</li>
                      <li><a href="{{action('AjudaController@index')}}" class="fa fa-question-circle-o fa-5x" title="ajuda">Ajuda</a>Ajuda</li>
                    @endif
                </ul>
        </nav>
        @yield('content')
    </div>


    <!-- Scripts -->
</body>
</html>
