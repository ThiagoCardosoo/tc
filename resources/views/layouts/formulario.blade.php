<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="Formulario" />
    <meta name="keywords" content="Questionário SQL" />
    <meta name="author" content="Thiago Cardoso" />
    <link href="{{URL::to('/css/bootstrap.css') }}" rel="stylesheet">
</head>
<body>

  @yield('contentform')

</body>
</html>
