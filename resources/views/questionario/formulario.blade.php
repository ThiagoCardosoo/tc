@extends('layouts.formulario')

@section('contentform')

 <div class="container">

         <header class="codrops-header">
             <h3>Questionário sobre o OA</h3>
         </header>

         <!-- /.row -->
         <div class="row">
         <div class="col-lg-12">
             <div class="panel panel-default">
                 <div class="panel-heading">Preencher Formulário</div>
                 <div class="panel-body">
                     <div class="table-responsive">
                       <form action="{{ url('questionario/enviar') }}" method="post"
                       data-toggle="validator" role="form">
                         <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                         <div class="form-group">
                           <label>1- Você já utilizou algum Objeto de aprendizagem(OA)?Se sim qual?</label>
                           <textarea class="form-control" maxlength="250" rows="5" id="comment" required></textarea>
                         </div>
                         <div class="form-group">
                           <label>2- Você já utilizou algum Objeto de aprendizagem(OA)?Se sim qual?</label>
                           <textarea class="form-control" maxlength="250" rows="5" id="comment" required></textarea>
                         </div>
                         <div class="form-group">
                           <label>3- Você já utilizou algum Objeto de aprendizagem(OA)?Se sim qual?</label>
                           <textarea class="form-control" maxlength="250" rows="5" id="comment" required></textarea>
                         </div>
                         <div class="form-group">
                           <label>4- Você já utilizou algum Objeto de aprendizagem(OA)?Se sim qual?</label>
                           <textarea class="form-control" maxlength="250" rows="5" id="comment" required></textarea>
                         </div>
                         <div class="form-group">
                           <label>5- Você já utilizou algum Objeto de aprendizagem(OA)?Se sim qual?</label>
                           <textarea class="form-control" maxlength="250" rows="5" id="comment" required></textarea>
                         </div>
                         <div class="form-group">
                           <label>6- Você já utilizou algum Objeto de aprendizagem(OA)?Se sim qual?</label>
                           <textarea class="form-control" maxlength="250" rows="5" id="comment" required></textarea>
                         </div>
                         <div class="checkbox">
                          <label><input type="checkbox" value="">Autorização para divulgação dos testes</label>
                        </div>
                           <button type="submit" class="btn btn-success btn-lg">Enviar Questionário</button>
                       </form>
                     <!-- /.table-responsive -->
                   </div>
                 <!-- /.panel-body -->
             </div>
             <!-- /.panel -->
         </div>
         <!-- /.col-lg-12 -->
         </div>

  </div>

@endsection
