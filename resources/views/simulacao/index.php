<!DOCTYPE html>
<html ng-app="drag-and-drop">
  <head lang="en">
    <meta charset="utf-8">
    <title>Simulação</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/ui-lightness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet">
     <link href="css/simulacao.css" rel="stylesheet" type="text/css">


    <script src="node_modules/angular/angular.js"></script>
    <script src="node_modules/angular-route/angular-route.js"></script>
    <script src="node_modules/sqlite-parser/dist/sqlite-parser.js"></script>

     <script src="app/angular-dragdrop.min.js"></script>
    <script src="app/controllers/simulacao.js"></script>
     <script src="js/jquery.bootstrap-growl.js"></script>

  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <style>
      .droppableArea { width: 600px !important; height:250px; }
      /*.btn-droppable { width: 180px; height: 40px; padding-left: 4px; }
      /*.btn-draggable { width: 160px; }*/
      /* Horizontal Sortable */
      .hthumbnail[jqyoui-droppable] { border: 1px solid red; }
      .hthumbnail {
        height: 50px;
        width: 50px;
        text-align: center;
        padding-top: 0px;
        cursor: pointer;
        background: rgb(182, 173, 123);
        position: relative;
        -webkit-transition: none;
        transition: none;
      }
      .hthumbnail.ng-leave {
        -webkit-transition-duration: 0s;
        transition-duration: 0s;
        opacity: 1;
      }
      .hthumbnail.ng-leave.ng-leave-active {
        opacity: 0;
      }
      .hthumbnail.ng-enter {
        -webkit-transition: left 0.3s;
        transition: left 0.3s;
      }
      .hthumbnail.ng-enter[data-direction="left"] {
        left: -80px; /* 60px width + 20px marginLeft */
      }
      .hthumbnail.ng-enter[data-direction="right"] {
        left: 80px; /* 60px width + 20px marginLeft */
      }
      .hthumbnail.ng-enter.ng-enter-active {
        left: 0px;
      }

      /* Vertical Sortable */
      .vthumbnail[jqyoui-droppable] { border: 1px solid red; }
      .vthumbnail {
        height: 50px;
        width: 50px;
        text-align: center;
        padding-top: 0px;
        cursor: pointer;
        background: rgb(182, 173, 123);
        position: relative;
        -webkit-transition: none;
        transition: none;
        float: none !important;
      }
      .vthumbnail.ng-leave {
        -webkit-transition-duration: 0s;
        transition-duration: 0s;
        opacity: 1;
      }
      .vthumbnail.ng-leave.ng-leave-active {
        opacity: 0;
      }
      .vthumbnail.ng-enter {
        -webkit-transition: top 0.3s;
        transition: top 0.3s;
      }
      .vthumbnail.ng-enter[data-direction="left"] {
        top: -76px; /* 56px width + 20px marginBottom */
      }
      .vthumbnail.ng-enter[data-direction="right"] {
        top: 76px; /* 56px width + 20px marginBottom */
      }
      .vthumbnail.ng-enter.ng-enter-active {
        top: 0px;
      }
    </style>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  </head>
  <body>


    <div id="divForError"></div>
    <!-- Tabela-->
    <div class="outerContainer" ng-controller="oneCtrl">


     <!--<tr ng-repeat="item in listc">
        <td>
               <div class="btn btn-primary" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="listc[$index].Field" jqyoui-draggable="{animate:true}" ng-hide="!item.Field">{{item.Field}}</div>
        </td>
        <td> <input type="button" ng-click="addRow($index)" value="Add" ng-show="$last"></td>
     </tr>
   </table> -->

        <div id="draggableContainer">
            <div id="exTab2" class="container">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#1" data-toggle="tab">Comandos</a>
                </li>
                <li>
                  <a href="#2" data-toggle="tab">Tabelas/Campos</a>
                </li>
              </ul>

              <div class="tab-content">
                <div class="tab-pane active" id="1">

                  <div class="navbar-inner">
                        <div class="btn btn-droppable" ng-repeat="item in list2" data-drop="true" ng-model='list2' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list2])',revert: 'invalid', helper: 'clone'}"  jqyoui-droppable="{index: {{$index}}, placeholder: 'keep'}">
                          <div class="btn btn-info btn-draggable itemMenu" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list2" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                        </div>
                        <button class="btn btn-success" data-toggle="modal" style="position:relative; top:20px; left:10px;" data-target="#myModalHelp" title="Ajuda"  type="button"><i class="fa fa-question-circle fa-2x" title="Ajuda" alt="Ajuda" aria-hidden="false"></i>Ajuda</button>
                        <div class="btn btn-droppable" style="position: relative; top:40px;" ng-repeat="item in listc3" data-drop="true"  data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=listc3])'}"  jqyoui-droppable="{index: {{$index}}}">
                          <div class="btn btn-info btn-draggable itemMenu" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="listc3" jqyoui-draggable="{index: {{$index}},placeholder:true,animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div><br />
                            <input type="text" ng-model="listc3[$index].Field" size="40" style="width: 95%; height: 40px;" placeholder="Comando"><br /><br />
                            <input type="button" class="btn btn-info btn-draggable" ng-click="addRowPontoText($index)" title="Adiciona um novo bloco de comando" value="Adicionar" ng-show="$last">
                            <input type="button" class="btn btn-info btn-draggable"  ng-click="deleteRowPontoText($event,$index)" value="Delete" ng-show="$index != 0">
                        </div>
                    </div>

                </div>
                <div class="tab-pane" id="2">


                    <div id="tabelContainer">
                      <div class="navbar-inner">

                     <div class="row">
                         <div class="col-md-12" style="margin-left:13px;">
                            <h5>Tabela Aluno</h5>
                         </div>
                        <div class="col-md-12">
                            <ul class="nav nav-stacked" id="accordion1">
                                <div class="">
                                  <div class="btn btn-droppable" ng-repeat="item in list4" data-drop="true" ng-model='list4' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list4])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                    <div class="btn btn-infoTabel btn-draggable itemMenu" title="Tabela Aluno" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list4" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                  </div>
                                <a data-toggle="collapse" data-parent="#accordion1" href="#firstLink"><i class="fa fa-caret-down" title="Campos da Tabela aluno"></i></a>
                                    <ul id="firstLink" class="collapse">
                                        <div class="btn btn-droppable" ng-repeat="item in list3" data-drop="true" ng-model='list3' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list3])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                          <div class="btn btn-info btn-draggable itemMenu" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list3" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                        </div>
                                    </ul>
                                </div>
                                <div class="">
                                  <div class="col-md-12">
                                     <h5>Tabela Disciplina</h5>
                                 </div>
                                  <div class="btn btn-droppable" ng-repeat="item in list5" data-drop="true" ng-model='list5' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list5])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                    <div class="btn btn-infoTabel btn-draggable itemMenu" title="Tabela Disciplina" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list5" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                  </div>
                                <a data-toggle="collapse" data-parent="#accordion1" href="#secondLink"><i class="fa fa-caret-down" title="Campos da Tabela Disciplina"></i></a>
                                    <ul id="secondLink" class="collapse">
                                        <div class="btn btn-droppable" ng-repeat="item in list6" data-drop="true" ng-model='list6' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list6])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}}}">
                                          <div class="btn btn-info btn-draggable itemMenu" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list6" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                        </div>
                                    </ul>
                                  </div>
                                <div class="">
                                  <div class="col-md-12">
                                     <h5>Tabela Turma</h5>
                                 </div>
                                 <div class="btn btn-droppable" ng-repeat="item in list7" data-drop="true" ng-model='list7' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list7])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                   <div class="btn btn-infoTabel btn-draggable itemMenu" title="Tabela Turma" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list7" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                 </div>
                                <a data-toggle="collapse" data-parent="#accordion1" href="#thirdLink"><i class="fa fa-caret-down" title="Campos da Tabela Turma"></i></a>
                                    <ul id="thirdLink" class="collapse">
                                        <div class="btn btn-droppable" ng-repeat="item in list8" data-drop="true" ng-model='list8' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list8])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                          <div class="btn btn-info btn-draggable itemMenu" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list8" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                       </div>
                                    </ul>
                                </div>
                                <div class="">

                                  <div class="col-md-12">
                                     <h5>Tabela HistoricoEscolar</h5>
                                 </div>
                                 <div class="btn btn-droppable" ng-repeat="item in list15" data-drop="true" ng-model='list15' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list15])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                   <div class="btn btn-infoTabel btn-draggable itemMenu" title="Tabela HistoricoEscolar" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list15" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                 </div>
                                 <a data-toggle="collapse" data-parent="#accordion1" href="#seventhLink"><i class="fa fa-caret-down" title="Campos da Tabela HistoricoEscolar"></i></a>
                                    <ul id="seventhLink" class="collapse">
                                        <div class="btn btn-droppable" ng-repeat="item in list16" data-drop="true" ng-model='list16' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list16])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                          <div class="btn btn-info btn-draggable itemMenu" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list16" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                       </div>
                                    </ul>
                                </div>
                                <div class="">
                                  <div class="col-md-12">
                                     <h5>Tabela PreRequisito</h5>
                                 </div>
                                  <div class="btn btn-droppable" ng-repeat="item in list17" data-drop="true" ng-model='list17' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list17])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                    <div class="btn btn-infoTabel btn-draggable itemMenu" title="Tabela PreRequisito" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list17" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                  </div>
                                <a data-toggle="collapse" data-parent="#accordion1" href="#ninthLink"><i class="fa fa-caret-down" title="Campos da Tabela PreRequisito"></i></a>
                                    <ul id="ninthLink" class="collapse">
                                        <div class="btn btn-droppable" ng-repeat="item in list18" data-drop="true" ng-model='list18' data-jqyoui-options="{accept:'.btn-draggable:not([ng-model=list18])',helper: 'clone'}"  jqyoui-droppable="{index: {{$index}},placeholder: 'keep'}">
                                          <div class="btn btn-info btn-draggable itemMenu" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="list18" jqyoui-draggable="{index: {{$index}},placeholder:'keep',animate:true, onStart:'startCallback(item, $index)', onStop: 'stopCallback', onDrag: 'dragCallback'}"ng-hide="!item.Field">{{item.Field}}</div>
                                       </div>
                                    </ul>
                                </div>
                            </ul>
                        </div>
                    </div>



                    </div>
                  </div>

                </div>
              </div>
            </div>

            <a  href="home"><button type="button" title="voltar" style="position:relative; top:70px;"  class="btn btn-success">Voltar</button></a>
        </div>

         <div id="droppableContainer" data-drop="true" ng-model='list1' data-jqyoui-options="optionsList1" jqyoui-droppable="{multiple:true, onDrop:'dropCallback',onOver: 'overCallback', onOut: 'outCallback'}" >
          <div class="caption">
            <div class="btn btn-info btn-draggable" ng-repeat="item in list1 track by $index"  ng-show="item.Field"  data-drop="true" data-drag="true" ng-model="list1" jqyoui-droppable="{index: {{$index}}, beforeDrop: 'beforeDrope'}" jqyoui-draggable="{index: {{$index}}, insertInline: true, direction:'jqyouiDirection'}" data-jqyoui-options="{revert: 'invalid'}" data-direction="{{item.jqyouiDirection}}" ng-dblclick="removeElement($index)">{{item.Field}}</div>
            <button class="btn btn-info btn-draggable" id="btnExecutar" ng-click="clickHandler();" type="button">Executar SQL</button>
          </div>

              <div class="modal fade" id="myModalHelp" role="dialog">
                  <div class="modal-dialogHelp">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Ajuda</h4>
                      </div>
                      <div class="modal-body">
                        <h4><b>Como usar a Simulação:</b></h4>
                        <p>
                          <b>Arraste e solte</b><br />
                          Na aba comandos estão os blocos onde se pode arrastar eles para a área do meio, Na aba
                          Tabela/Campos estão as tabelas e os campos que tambem possuem a opção de arrastar para a área do meio,
                          ao arrastar e largar na área do meio os blocos vai se montando a instrução SQL.<br /><br />
                          <b>Ordenação</b>
                          <br />Para a ordenação dos blocos é necessario arrastar o mesmo para a direita ou esquerda, sempre que um bloco é adicionado ele vai para o fim. Por Exemplo para mover um bloco no meio de outros é necessario arrastar esse bloco e largar no meio, porém esse bloco vai ir para o fim, então deve-se mover para o meio no local desejado.<br /><br />
                          <b>Sintaxe Permitida</b>
                          Utiliza-se das seguintes sintaxes:<br />
                          SELECT * FROM Tabela.<br /><br />
                          SELECT campo,campo FROM Tabela.<br /><br />
                          SELECT * FROM TabelaEsquerda INNER JOIN<br />
                          TabelaDireita ON TabelaEsquerda.campo = TabelaDireita.campo.<br /><br />
                          SELECT TabelaEsquerda.campo,TabelaDireita.Campo FROM TabelaEsquerda INNER JOIN<br />
                          TabelaDireita ON TabelaEsquerda.campo = TabelaDireita.campo.<br /><br />
                          SELECT TabelaEsquerda.campo,TabelaDireita.Campo FROM TabelaEsquerda INNER JOIN<br />
                          TabelaDireita ON TabelaEsquerda.campo = TabelaDireita.campo WHERE TabelaEsquerda.campo = condicao.<br /><br />
                          SELECT * FROM Tabela WHERE campo = condição.<br /><br />
                          SELECT * FROM TabelaEsquerda,TabelaDireita WHERE TabelaEsquerda.campo = TabelaDireita.Campo.<br /><br />
                          OBS: É permitido junção somente entre 2 tabelas.
                          <br /><br /><b>Remoção de um bloco</b><br />
                          Para remover Blocos de comando (É preciso dar dois cliques em cima do bloco de comando).<br /><br />
                          <b>Adicionar comando Personalizado</b><br />
                          Para adicionar um novo comando existe o botão (Adicionar) na aba comandos onde é possivel
                          criar um novo comando para por numa condição WHERE por exemplo. WHERE Aluno.CodAluno = 1(Esse 1 foi criado personalizado).<br /><br />
                          <b>Botões com funcionalidades:</b><br />
                          Botão Visualizar SQL: É possivel visualizar o comando SQL da instrução montada.<br/>
                          Botão Resetar: Remove todos os blocos da tela.<br />
                          Botão Exemplo 1: Simulação Exemplo para ver a animação da simulação de uma junção entre a tabela Aluno e HistoricoEscolar.<br />
                          Botão Exemplo 2: Simulação Exemplo para ver a animação da simulação de uma junção entre a tabela Disciplina e Turma com o uso de WHERE.<br />
                          Botão Executar: Botão utilizado para ver na tela o a instrução SQL formada após a montagem dos blocos, onde o mesmo apresenta
                          a Tabela (esquerda), Tabela (direita), Tabela com o resultado da Junção entre a Tabela (esquerda) e Tabela (direita).<br />
                          Botão Simulação: Botão utilizado para ver a animação da simulação feita após se montar a instrução SQL da junção com a sintaxe permitida correta, apresenta novos botões para seguir passo a passo, retroceder e botão de simulação (automático).<br /><br />
                          <b>Exercícios de SQL</b>
                          Serão apresentados exercicios SQL para estudo e pratica do OA, sendo possivel se desejado montar instruções diferentes das apresentadas.
                          <br />Exercicios de junção inner join Números: 6,7,8,10,13,14
                        </p>
                      </div>
                    </div>
                  </div>
                </div>


            <div id="tabelas_container">
                    <!--  <div ng-show="true" style="color: white; font-size: 25px; font-weight bold; position: relative; top: 100px;">{{tabelaIndividual}}</div>-->
                      <div ng-show="true" style="color: white; font-size: 25px; font-weight bold; margin-top:100px;">{{tabelaEsquerda}}</div>
                      <table  class="table table-condensed table-bordered" id="tabela1" style="background-color: #f1f1f1">
                        <thead>
                          <tr>
                            <th ng-repeat="(header, value) in resultData4[0]" style="background-color: #a167db; color: white;">
                              {{header}}
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="row in resultData4"  ng-class="{'selected':$index == selectedRow}" ng-click="setClickedRow($index)"  style="color: #a167db;">
                            <td ng-repeat="cell in row">
                              {{cell}}
                            </td>
                          </tr>
                        </tbody>
                      </table>


                      <div ng-show="true" style="color: white; font-size: 25px; font-weight bold;">{{tabelaDireita}}</div>
                      <table class="table table-condensed table-bordered" id="tabela2" style="background-color: #f1f1f1" ng-keydown="key($event)">
                        <thead>
                          <tr>
                            <th ng-repeat="(header, value) in resultData5[0]" style="background-color: #18685c; color: white;">
                              {{header}}
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="row in resultData5" ng-class="{'selected':$index == selectedRowSecond}" ng-click="setClickedRowSecond($index)"  style="color: #18685c;">
                            <td ng-repeat="cell in row">
                              {{cell}}
                            </td>
                          </tr>
                        </tbody>
                      </table>
          </div>


                      <div style="padding-bottom:10px;">
                        <button id="goback" type="button" ng-click="stepDown()" class="btn btn-default"><span class="fa fa-chevron-left"></span></button>
                        <button id="advance" type="button" ng-click="stepUp()" class="btn btn-default"><span class="fa fa-chevron-right"></span></button>
                        <button id="immediate" type="button" ng-click="imediateSimulation()" class="btn btn-default"><span class="fa fa-play-circle"></span></button>
                      </div>

                       <button class="btn btn-info btn-draggable" id="btnSimulacao" style="margin-bottom: 10px; display: block;" ng-click="btnSimulation();" title="Botão para executar a simulação da animação" type="button">Simular</button><br />

                       <div class="modal fade" id="myModal" role="dialog">
                         <div class="modal-dialog">

                           <!-- Modal content-->
                           <div class="modal-content">
                             <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                               <h4 class="modal-title">LOG SQL</h4>
                             </div>
                             <div class="modal-body">
                               <p>{{query}}</p>
                             </div>
                           </div>
                         </div>
                       </div>

                       <table  class="table table-condensed table-bordered" id="tabelasimula"  style=" background-color: #f1f1f1; width: 450px; float: left;">
                         <thead>
                           <tr>
                             <th ng-repeat="(header, value) in resultData7[0]" style="background-color: #a167db; color: white;">
                               {{header}}
                             </th>
                           </tr>
                         </thead>
                         <tbody>
                           <tr ng-repeat="row in resultData7"   ng-click="setClickedRow($index)">
                             <td ng-repeat="(key, val) in row"   ng-style="{ 'background-color': color(key,val) }">
                               {{val}}
                             </td>
                           </tr>
                         </tbody>
                       </table>


                       <table class="table table-condensed table-bordered border:1px solid red;" style="width: 30%; float: left;">
                         <thead>
                           <tr>
                             <th ng-repeat="(header, value) in resultData8[0]" style="background-color: #18685c; color: white;">
                               {{header}}
                             </th>
                           </tr>
                         </thead>
                         <tbody>
                           <tr ng-repeat="row in resultData8"  ng-click="setClickedRowSecond($index)" style="color: #18685c;">
                             <td ng-repeat="(key, val) in row"   ng-style="{ 'background-color': color(key,val) }" style="background-color: #f1f1f1">
                               {{val}}
                             </td>
                           </tr>
                         </tbody>
                       </table>

                       <div class="clear"></div>
                       <div ng-show="true" id="resultInner" style="color: white; font-size: 25px; font-weight bold; margin-bottom: -30px;">{{resultInner}}</div>
                       <table style="500px;" class="table table-condensed table-bordered" id="tabelaResult">
                         <thead>
                           <tr>
                             <th ng-repeat="(header, value) in resultData[0]" class="table-tr">
                               {{header}}
                             </th>
                           </tr>
                         </thead>
                         <tbody>
                           <tr ng-repeat="row in resultData" ng-class-odd="'odd'" ng-class-even="'even'">
                             <td ng-repeat="(key, val) in row"   ng-style="{ 'background-color': colorAlternate(key,val) }">
                               {{val}}
                             </td>
                           </tr>
                         </tbody>
                       </table>

              <button class="btn btn-info btn-draggable" id="btnLogSQL"  data-toggle="modal" data-target="#myModal" style="margin-bottom:20px;" type="button">Visualizar SQL</button>



                <button class="btn btn-info btn-draggable" id="btnExecutar" ng-click="clickHandlerExample2();" title="Exemplo de Simulação com Junção entre as tabelas Disciplina e Turma" type="button">Exemplo 2</button>
                  <button class="btn btn-info btn-draggable" id="btnExecutar" ng-click="clickHandlerExample1();" title="Exemplo de Simulação com Junção entre as tabelas Aluno e HistoricoEscolar"  type="button">Exemplo 1</button>
                <a href="simulacao"><button class="btn btn-info btn-draggable" type="button">Resetar</button></a><br /><br />
                <div style="color: black;  font-size: 15px; font-weight bold;">
                  <b>Simulação com Junção</b> <br />

                  <table class="table table-condensed table-bordered" style="background-color: #f1f1f1">
                    <tr>
                      <td colspan="2"><b>Exercício de SQL</b></td>
                    </tr>
                   <tr>
                       <th>Id</th>
                       <th>Exercicio</th>
                   </tr>
                   <?php
                   foreach ($arrExercicios as $exercicio){
                   ?>
                   <tr>
                       <td><?php echo $exercicio->idExercicio ?></td>
                       <td><?php echo $exercicio->exercicio ?></td>
                   </tr>
                   <?php
                    }
                   ?>
                  </table>
              </div>
             <div style="position:relative; bottom: 30px;"><?php echo $arrExercicios->links() ?></div>
        </div>
    </div>

    <div class="row" style="margin-right: 20px;">
        <div class="alert alert-danger hidden" role="alert" style="position: absolute; bottom: 10px; left: 610px; width: 35%;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{alert}}</strong><div id="divForErrorQuery"></div>
        </div>
    </div>

    <!--  <a  href="javascript:window.history.go(-1)"><button type="button" id="btnVoltar" class="btn btn-success">Voltar</button></a>-->



 <style type="text/css">
    .selected {
      background-color: #7ee5a2;
      color: white;
      font-weight: bold;
    }
    .selectedrow {
      font-weight: 10px;
      background-color: #7ee5a2;
      color: #18685c;
      font-weight: normal;
    }
    table, th , td  {
  border: 1px solid grey;
  border-collapse: collapse;
  padding: 5px;

  font-family:arial;
  }
  .odd {
  background-color: #ffffff;
  }
  .even {
    background-color: #f1f1f1;
  }
  div.clear { clear: both; }

</style>

  </body>
</html>
