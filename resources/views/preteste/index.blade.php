@extends('layouts.inicio')

@section('content')

<!-- scripts menu -->
 @include('layouts.script')

 <link href="css/zui.css" rel="stylesheet" type="text/css">

 <div class="container-preteste">
             <div class="titleOA">
                 <header class="codrops-header">

                 <h1>Exercicio - Pré-Teste</h1>
                 </header>
                 <div class="container-middle">
                   <ul class="well" style="padding-left: 3em;">
                     @if(Session::has('question-message'))
                         <div class="alert alert-warning">
                             <button type="button"
                                 class="close"
                                 data-dismiss="alert"
                                 aria-hidden="true">&times;</button>
                             {!! session()->get('question-message') !!}<br />
                             {!! session()->get('question-message1') !!}<br />
                             {!! session()->get('question-message2') !!}<br />
                             {!! session()->get('question-message3') !!}<br />
                             {!! session()->get('question-message4') !!}<br />
                             {!! session()->get('question-message5') !!}<br />
                             {!! session()->get('question-message6') !!}<br />
                             {!! session()->get('question-message7') !!}<br />
                             {!! session()->get('question-message8') !!}<br />
                             {!! session()->get('question-message9') !!}<br />
                             {!! session()->get('question-message10') !!}<br /><br />
                             <b>{!! session()->get('question-message11') !!}</b><br />
                              {!! session()->get('question-message12') !!}<br /><br />
                         </div>
                     @endif
                     <p>
                     Antes de seguir para os Modulos vamos testar seu conhecimento em Banco de dados e em alguns conceitos da disciplina de Organização de Banco de Dados como:
                     Modelo Relacional, Chave, Campos, Tabelas, Comandos SQL, Álgebra relacional.
                     </p>
                     <p>Marque uma unica alternativa para cada questão. (Todas as questões são obrigatorias o preenchimento).</p>
                     <form action="{{ url('preteste/store') }}" method="post"
                      data-toggle="validator" role="form">
                       <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                       <div class="form-group">
                         <?php
                           foreach ($arrPreTeste as $key => $value) {
                              $CodQuestao = $value->CodQuestao;
                              $Descricao = $value->descricao;
                         ?>
                         <label><b><?php echo $key + 1 ?> - <?php echo $Descricao ?> </b><br /></label><br />
                          <?php
                            $arrPreTesteOpcoesQuestao1 =  DB::table('pre_teste_opcoes')->where('CodQuestao', '=', $CodQuestao)->get();

                              foreach ($arrPreTesteOpcoesQuestao1 as $key => $value) {
                                $OpcaoA = $value->OpcaoA;
                                $OpcaoB = $value->OpcaoB;
                                $OpcaoC = $value->OpcaoC;
                                $OpcaoD = $value->OpcaoD;
                           ?>
                         <div class="col-md-12">

                             <div class="funkyradio">
                                <input type="hidden" name="CodQuestao[<?php echo $CodQuestao ?>]" value="<?php echo $CodQuestao ?>"  />
                                 <div class="funkyradio-default">
                                     <input type="radio" name="Questao[<?php echo $CodQuestao ?>]" value="A" id="radio1" required />
                                     <label for="radio1"><?php echo $OpcaoA ?></label>
                                 </div>
                                 <div class="funkyradio-primary">
                                     <input type="radio" name="Questao[<?php echo $CodQuestao ?>]" value="B" id="radio2" required />
                                     <label for="radio2"><?php echo $OpcaoB ?></label>
                                 </div>
                                 <div class="funkyradio-success">
                                     <input type="radio" name="Questao[<?php echo $CodQuestao ?>]" value="C" id="radio3" required />
                                     <label for="radio3"><?php echo $OpcaoC ?></label>
                                 </div>
                                 <div class="funkyradio-danger">
                                     <input type="radio" name="Questao[<?php echo $CodQuestao ?>]" value="D" id="radio4" required />
                                     <label for="radio4"><?php echo $OpcaoD ?></label>
                                 </div><br />
                             </div>
                             <?php
                                }
                             ?>
                        <?php
                          }

                       ?>
                       </div><br />
                         <button type="submit" class="btn btn-success btn-lg">Enviar</button>
                     </form>
                   </ul>
                 </div>
             </div>
        </div>
 </div>

 <div class="btnVoltar-preteste">
   <a  href="javascript:window.history.go(-1)"><button type="button" class="btn btn-success">Voltar</button></a>
 </div>

 <style>
 .btn-success {
     color: #fff;
     text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
     background-color: #5bb75b;
     background-image: -moz-linear-gradient(top,#62c462,#51a351);
     background-image: -webkit-gradient(linear,0 0,0 100%,from(#62c462),to(#51a351));
     background-image: -webkit-linear-gradient(top,#62c462,#51a351);
     background-image: -o-linear-gradient(top,#62c462,#51a351);
     background-image: linear-gradient(to bottom,#62c462,#51a351);
     background-repeat: repeat-x;
     border-color: #51a351 #51a351 #387038;
     border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
     filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462',endColorstr='#ff51a351',GradientType=0);
     filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
 }
 </style>

@endsection
