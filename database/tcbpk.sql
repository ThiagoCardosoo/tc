-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20-Abr-2017 às 04:10
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `modelo_laravel`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nivel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Loja_idLoja` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Aluno`
--

CREATE TABLE `Aluno` (
  `CodAluno` int(11) NOT NULL,
  `NomeAluno` varchar(25) DEFAULT NULL,
  `Matricula` varchar(25) DEFAULT NULL,
  `Curso` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Aluno`
--

INSERT INTO `Aluno` (`CodAluno`, `NomeAluno`, `Matricula`, `Curso`) VALUES
(1, 'Pedro', '73580', 'Ciência da Computação'),
(2, 'Gabriel', '72340', 'Licenciatura em Computação'),
(3, 'Fulano', '7567', 'Ciência da Computação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `back_produtos`
--

CREATE TABLE `back_produtos` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `valor` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `back_produtos`
--

INSERT INTO `back_produtos` (`ID`, `nome`, `valor`, `descricao`, `quantidade`) VALUES
(1, 'Geladeira', '5900.00', 'Side by Side com gelo na porta', 2),
(2, 'FogÃ£o', '950.00', 'Painel automÃ¡tico e forno elÃ©trico', 5),
(3, 'Microondas', '1520.00', 'Manda SMS quando termina de esquentar', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE `contatos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `posicao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `nome`, `email`, `telefone`, `posicao`, `created_at`, `updated_at`) VALUES
(1, 'Earl Dach', 'lcorkery@example.net', 'Vel provident non saepe sint eius ipsam.', '41', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(2, 'Granville Renner', 'ukoepp@example.org', 'Nobis molestias molestias explicabo placeat voluptatem omnis ut.', '23', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(3, 'Abelardo McClure', 'rice.caroline@example.org', 'Ipsum deserunt aut quos quidem.', '13', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(4, 'Dr. Kory VonRueden II', 'hilll.jalen@example.net', 'Voluptatum voluptatum occaecati corrupti non.', '25', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(5, 'Cullen McDermott', 'mante.roberta@example.com', 'Amet molestiae minus eaque voluptas vero deleniti.', '25', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(6, 'Maya Lueilwitz', 'paucek.art@example.org', 'Velit rerum tempore dolor quo dicta.', '37', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(7, 'Prof. Luz Ortiz IV', 'janis.rodriguez@example.com', 'Eum similique quia necessitatibus accusamus.', '10', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(8, 'Avis Jakubowski PhD', 'runolfsdottir.marjolaine@example.com', 'Sint ipsam eligendi et ipsa.', '41', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(9, 'Jules Jaskolski DVM', 'cortney47@example.com', 'Omnis nemo voluptatem laudantium vero quia minus.', '23', '2017-01-03 01:52:41', '2017-01-03 01:52:41'),
(10, 'Aisha Shields', 'lesch.kelton@example.com', 'Consequatur enim voluptatibus tempore at.', '46', '2017-01-03 01:52:41', '2017-01-03 01:52:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Disciplina`
--

CREATE TABLE `Disciplina` (
  `CodDisciplina` int(11) NOT NULL,
  `NomeDisciplina` varchar(40) DEFAULT NULL,
  `Credito` varchar(25) DEFAULT NULL,
  `Departamento` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Disciplina`
--

INSERT INTO `Disciplina` (`CodDisciplina`, `NomeDisciplina`, `Credito`, `Departamento`) VALUES
(1, 'Organização de Banco de dados', '4', 'Ciência da Computação'),
(2, 'Algoritmo', '4', 'Ciência da Computação'),
(3, 'Estrutura de Dados', '4', 'Ciência da Computação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `exercicios`
--

CREATE TABLE `exercicios` (
  `idExercicio` int(11) NOT NULL,
  `exercicio` varchar(450) NOT NULL,
  `modulo` int(11) NOT NULL,
  `status` enum('S','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `exercicios`
--

INSERT INTO `exercicios` (`idExercicio`, `exercicio`, `modulo`) VALUES
(1, 'Mostrar todas as Disciplinas.\r\n', 1),
(2, 'Mostrar todos os Alunos.\r\n', 1),
(3, 'Mostrar todas as Turmas.\r\n', 1),
(4, 'Mostrar os campos CodAluno,NomeAluno da tabela Aluno.\r\n', 1),
(5, 'Mostrar todas as Turmas (todos os campos) do Professor Jorge', 1),
(6, 'Mostrar todos os Alunos e as Turmas que os Alunos pertencem  (Analisar a Simulação da junção feita).\r\n', 1),
(7, 'Mostrar o Nome(NomeAluno) e a Matricula(Matricula)  de todas as Turmas.\r\n', 1),
(8, ' Mostrar as Disciplinas e a Turma que o professor 1 pertence.(Analisar a Simulação da junção feita).\r\n', 1),
(9, ' Mostrar todo o Histórico escolar da universidade.\r\n', 1),
(10, 'Mostrar o Histórico escolar de todos os Alunos. (Analisar a Simulação da junção feita).\r\n', 1),
(11, 'Mostrar os Alunos e suas Matrículas.\r\n', 1),
(12, ' Mostrar todos os cursos da universidade\r\n', 1),
(13, 'Mostrar CodDisciplina, NomeDisciplina , Credito , CodPreRequisito para o pre requisito de todas as Disciplinas.\r\n(Analisar a Simulação da junção feita). \r\n', 1),
(14, 'Mostrar CodTurma, professor, ano, nota  que pertence o histórico escolar da Turma.\r\n(Analisar a Simulação da junção feita). \r\n', 1),
(15, 'Mostrar o credito e o departamento da Disciplina Algoritmo\r\n', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `HistoricoEscolar`
--

CREATE TABLE `HistoricoEscolar` (
  `CodAluno` int(11) DEFAULT NULL,
  `CodTurma` int(11) DEFAULT NULL,
  `Nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `HistoricoEscolar`
--

INSERT INTO `HistoricoEscolar` (`CodAluno`, `CodTurma`, `Nota`) VALUES
(1, 1, 8),
(1, 2, 10),
(2, 2, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_12_26_041251_create_produtos_table', 2),
('2016_12_26_042223_create_produtos_table', 3),
('2016_12_07_064650_create_contatos_table', 4),
('2017_02_15_112322_create_admin_users_table', 5),
('2017_02_23_024636_create_sessions_table', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `PreRequisito`
--

CREATE TABLE `PreRequisito` (
  `CodPreRequisito` int(11) NOT NULL,
  `CodDisciplina` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `PreRequisito`
--

INSERT INTO `PreRequisito` (`CodPreRequisito`, `CodDisciplina`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` decimal(5,2) NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantidade` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `valor`, `descricao`, `quantidade`, `created_at`, `updated_at`) VALUES
(1, 'Geladeira2', '50.00', 'Side by Side com gelo na porta', 2, NULL, NULL),
(2, 'Produto teste', '555.00', 'Produto para teste', 34, NULL, NULL),
(3, 'Produto teste2', '3.00', 'Produto para teste2', 43, NULL, NULL),
(4, 'Produto teste3', '23.00', 'Produto para teste3', 5, NULL, NULL),
(5, 'Produto teste4', '77.00', 'Produto para teste4', 6, NULL, NULL),
(6, 'Geladeira', '50.00', 'Side by Side com gelo na porta', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Turma`
--

CREATE TABLE `Turma` (
  `CodTurma` int(11) NOT NULL,
  `CodDisciplina` int(11) DEFAULT NULL,
  `Semestre` varchar(25) DEFAULT NULL,
  `Ano` decimal(8,0) DEFAULT NULL,
  `Professor` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Turma`
--

INSERT INTO `Turma` (`CodTurma`, `CodDisciplina`, `Semestre`, `Ano`, `Professor`) VALUES
(1, 1, '10', '2016', 'Jorge'),
(2, 2, '10', '2016', 'Professor2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countError` int(11) NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `countError`, `created_at`, `updated_at`) VALUES
(3, 'Thiago', 'thiagocardosooti@gmail.com', '$2y$10$ODPJhQWYVnc6M6hN3F5zCOlQm3kVszI/0/nw34NT98lH9x8rQD86a', 'ENSv1pGU0Uac5X4bU2hIRuxYL6RxFBUEnoHU6UEWm9IrdnN07PIP9QGAbpiy', 0, '2016-12-28 02:32:19', '2017-04-17 04:33:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_email_unique` (`email`);

--
-- Indexes for table `Aluno`
--
ALTER TABLE `Aluno`
  ADD PRIMARY KEY (`CodAluno`);

--
-- Indexes for table `contatos`
--
ALTER TABLE `contatos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contatos_nome_unique` (`nome`),
  ADD UNIQUE KEY `contatos_email_unique` (`email`);

--
-- Indexes for table `Disciplina`
--
ALTER TABLE `Disciplina`
  ADD PRIMARY KEY (`CodDisciplina`);

--
-- Indexes for table `exercicios`
--
ALTER TABLE `exercicios`
  ADD PRIMARY KEY (`idExercicio`);

--
-- Indexes for table `HistoricoEscolar`
--
ALTER TABLE `HistoricoEscolar`
  ADD KEY `FK_Aluno_HistoricoEscolar` (`CodAluno`),
  ADD KEY `FK_Turma_HistoricoEscolar` (`CodTurma`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `PreRequisito`
--
ALTER TABLE `PreRequisito`
  ADD PRIMARY KEY (`CodPreRequisito`),
  ADD KEY `FK_Disciplina_PreRequisito` (`CodDisciplina`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `Turma`
--
ALTER TABLE `Turma`
  ADD PRIMARY KEY (`CodTurma`),
  ADD KEY `FK_Disciplina_Turma` (`CodDisciplina`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contatos`
--
ALTER TABLE `contatos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `exercicios`
--
ALTER TABLE `exercicios`
  MODIFY `idExercicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `HistoricoEscolar`
--
ALTER TABLE `HistoricoEscolar`
  ADD CONSTRAINT `FK_Aluno_HistoricoEscolar` FOREIGN KEY (`CodAluno`) REFERENCES `Aluno` (`CodAluno`),
  ADD CONSTRAINT `FK_Turma_HistoricoEscolar` FOREIGN KEY (`CodTurma`) REFERENCES `Turma` (`CodTurma`);

--
-- Limitadores para a tabela `PreRequisito`
--
ALTER TABLE `PreRequisito`
  ADD CONSTRAINT `FK_Disciplina_PreRequisito` FOREIGN KEY (`CodDisciplina`) REFERENCES `Disciplina` (`CodDisciplina`);

--
-- Limitadores para a tabela `Turma`
--
ALTER TABLE `Turma`
  ADD CONSTRAINT `FK_Disciplina_Turma` FOREIGN KEY (`CodDisciplina`) REFERENCES `Disciplina` (`CodDisciplina`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
