-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Maio-2017 às 23:25
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_oa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nivel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Loja_idLoja` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE `aluno` (
  `CodAluno` int(11) NOT NULL,
  `NomeAluno` varchar(25) DEFAULT NULL,
  `Matricula` varchar(25) DEFAULT NULL,
  `Curso` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`CodAluno`, `NomeAluno`, `Matricula`, `Curso`) VALUES
(1, 'Pedro', '73580', 'Ciência da Computação'),
(2, 'Gabriel', '72340', 'Licenciatura em Computação'),
(3, 'Fulano', '7567', 'Ciência da Computação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

CREATE TABLE `disciplina` (
  `CodDisciplina` int(11) NOT NULL,
  `NomeDisciplina` varchar(40) DEFAULT NULL,
  `Credito` varchar(25) DEFAULT NULL,
  `Departamento` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`CodDisciplina`, `NomeDisciplina`, `Credito`, `Departamento`) VALUES
(1, 'Organização de Banco de dados', '4', 'Ciência da Computação'),
(2, 'Algoritmo', '4', 'Ciência da Computação'),
(3, 'Estrutura de Dados', '4', 'Ciência da Computação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `exercicios`
--

CREATE TABLE `exercicios` (
  `idExercicio` int(11) NOT NULL,
  `exercicio` varchar(450) NOT NULL,
  `modulo` int(11) NOT NULL,
  `status` enum('S','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `exercicios`
--

INSERT INTO `exercicios` (`idExercicio`, `exercicio`, `modulo`, `status`) VALUES
(1, 'Mostrar todos os campos da tabela  Disciplina.\r\n\r\n\r\n', 1, 'S'),
(2, 'Mostrar todos os campos da tabela Aluno.\r\n\r\n', 1, 'S'),
(3, 'Mostrar todos os campos da tabela Turma.\r\n', 1, 'S'),
(4, 'Mostrar os campos CodAluno,NomeAluno da tabela Aluno.\r\n', 1, 'S'),
(5, 'Mostrar todos os campos da tabela Turma onde Professor = Jorge', 1, 'S'),
(6, 'Mostrar os campos CodAluno,Curso da tabela Aluno e os campos CodAluno,CodTurma,Nota da tabela HistoricoEscolar com junção feita entre as duas tabelas (Analisar a Simulação da junção feita).', 1, 'S'),
(7, 'Mostrar os campos NomeAluno, Matricula  da tabela Aluno.', 1, 'S'),
(8, ' Mostrar os campos CodDisciplina,NomeDisciplina da tabela Disciplina e os campos CodDisciplina,CodTurma,Professor da tabela Turma\r\ncom junção feita entre as duas tabelas\r\n(Analisar a Simulação da junção feita).', 1, 'S'),
(9, 'Mostrar todos os campos da tabela HistóricoEscolar', 1, 'S'),
(10, 'Mostrar todos os campos da tabela Aluno e da tabela HistoricoEscolar\r\ncom junção feita entre as duas tabelas (Analisar a Simulação da junção feita).', 1, 'S'),
(11, 'Mostrar os campos NomeAluno,Matricula da tabela Aluno.\r\n', 1, 'S'),
(12, ' Mostrar o campo curso da tabela Aluno', 1, 'S'),
(13, 'Mostrar os campos CodDisciplina, NomeDisciplina , Credito da tabela Disciplina e os campos CodDisciplina, CodPreRequisito da tabela Prequisito\r\ncom junção feita entre as duas tabelas\r\n(Analisar a Simulação da junção feita).', 1, 'S'),
(14, 'Mostrar os campos CodTurma, Professor, Ano da tabela Turma e os campos CodTurma, CodAluno, Nota da tabela HistoricoEscolar com uma Junção feita na tabela HistoricoEscolar. (Analisar a Simulação da junção feita).', 1, 'S'),
(15, 'Mostrar os campo credito, departamento da\r\ntabela disciplina onde NomeDisciplina = Algoritmo\r\n', 1, 'S');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historicoescolar`
--

CREATE TABLE `historicoescolar` (
  `CodAluno` int(11) DEFAULT NULL,
  `CodTurma` int(11) DEFAULT NULL,
  `Nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `historicoescolar`
--

INSERT INTO `historicoescolar` (`CodAluno`, `CodTurma`, `Nota`) VALUES
(1, 1, 8),
(1, 2, 10),
(2, 2, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_12_26_041251_create_produtos_table', 2),
('2016_12_26_042223_create_produtos_table', 3),
('2016_12_07_064650_create_contatos_table', 4),
('2017_02_15_112322_create_admin_users_table', 5),
('2017_02_23_024636_create_sessions_table', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prerequisito`
--

CREATE TABLE `prerequisito` (
  `CodPreRequisito` int(11) NOT NULL,
  `CodDisciplina` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `prerequisito`
--

INSERT INTO `prerequisito` (`CodPreRequisito`, `CodDisciplina`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pre_teste`
--

CREATE TABLE `pre_teste` (
  `CodPreteste` int(11) NOT NULL,
  `CodQuestao` int(11) NOT NULL,
  `descricao` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pre_teste`
--

INSERT INTO `pre_teste` (`CodPreteste`, `CodQuestao`, `descricao`) VALUES
(1, 1, 'Qual das seguintes instruções compõem um comando SQL?\r\n'),
(2, 2, 'Qual a necessidade de se usar a PK(chave primária)?\r\n'),
(3, 3, 'Para que serve a instrução Select?\r\n'),
(4, 4, 'O que são operadores lógicos?(AND,OR,NOT)\r\n'),
(5, 5, 'Quais são os operadores de comparação?\r\n'),
(6, 6, 'O que são entidades?\r\n'),
(7, 7, 'Qual a composição de um banco de dados relacional?\r\n'),
(8, 8, ' Sobre a Álgebra relacional é correto afirmar que:?\r\n'),
(9, 9, ' Qual a funcionalidade da junção na Álgebra relacional?\r\n'),
(10, 10, 'A junção de duas tabelas ou mais pode ser realizada através de quais comandos ?\r\n');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pre_teste_opcoes`
--

CREATE TABLE `pre_teste_opcoes` (
  `CodPretesteOp` int(11) NOT NULL,
  `CodQuestao` int(11) NOT NULL,
  `OpcaoA` varchar(450) NOT NULL,
  `OpcaoB` varchar(450) NOT NULL,
  `OpcaoC` varchar(450) NOT NULL,
  `OpcaoD` varchar(450) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pre_teste_opcoes`
--

INSERT INTO `pre_teste_opcoes` (`CodPretesteOp`, `CodQuestao`, `OpcaoA`, `OpcaoB`, `OpcaoC`, `OpcaoD`) VALUES
(1, 1, 'From alunos\r\n', 'Select PrimeiroNome\r\n', 'Select PrimeiroNome From Aluno', 'Select Aluno'),
(2, 2, 'A chave primária PK serve para identificar linhas  permitindo que o objeto seja identificado de forma única na tabela.\r\n', 'A chave primária PK – Primary Key serve para identificar uma entidade ou seja, permitir que o objeto seja identificado de forma única. Para isso, quando um campo é marcado como PK o banco não permitirá que valores duplicados sejam gravados ali.', 'A chave primária PK serve para identificar um objeto que está sendo usado ou referenciado em outra tabela.\r\n', 'A chave primária serve para se identificar uma consulta feita.\r\n\r\n'),
(3, 3, 'Para definir a ordem das consultas.\r\n', 'Para selecionar um campo em específico da tabela.', 'Para retornar os últimos registros de uma consulta.\r\n', 'Para definir os campos da consulta.\r\n\r\n'),
(4, 4, 'São operadores tradicionais da matemática que podem ser usados em expressões SQL.\r\n', 'São condições para se fazer comparações a uma expressão ou valor.\r\n', 'São operadores que fazem com que duas condições  tenham de produzir um resultado único.', 'São operadores que ajudam a recuperar o resultado de uma consulta.'),
(5, 5, '+, - , * , /\r\n', 'And,Or,Not\r\n', ' =, > , >= , < , <=, *\r\n', 'Sum,Avg,Count\r\n'),
(6, 6, 'É representada no modelo conceitual por um conjunto de objetos sobre os quais se obtêm informações no banco de dados.', 'É um conjunto de objeto no modelo conceitual que permite a especificação das propriedades dos objetos que serão armazenados no BD (Banco de dados).', 'São usadas para associar a informação a ocorrência de relacionamentos.\r\n', ' São um conjunto formado por valores.'),
(7, 7, 'Um banco de dados relacional é composto de chaves, valores, entidades.\r\n', 'Um banco de dados relacional é composto por tabelas que são composta por linhas chamado de duplas, cada linha possui uma série de campos(valor de atributo) .Cada campo  é identificado por nome de campo(nome de atributo) e um conjunto de campos de linhas formam uma coluna.', 'Um banco de dados relacional é composto por tabelas que são composta por valores\r\nque representam colunas.\r\n', 'Um banco de dados relacional é composto por uma expressão de cálculo relacional,\r\na qual gera uma nova relação para especificar como recuperar o resultado da consulta.\r\n'),
(8, 8, 'É uma tabela virtual no banco de dados.', 'É um conjunto utilizado para unir os dados.\r\n', 'São expressões que retornam e juntam os dados.\r\n', 'É o conjunto base de operações para o modelo relacional utilizado para manipular as relações.'),
(9, 9, 'Seleciona um conjunto determinado de colunas de uma relação.', 'Retorna todas as combinações de tuplas de duas tabelas.', 'Retorna a combinação de tuplas de duas relações de duas  tabelas.', 'Não é utilizada na álgebra relacional.'),
(10, 10, 'Select,*,From,Where.', 'Inner join, * , Left Join, Right Join.', 'Tabela.Campo, Campo, *.', 'Inner Join, Left Join, Right Join, Outer full Join, Cross Join.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pre_teste_resposta`
--

CREATE TABLE `pre_teste_resposta` (
  `CodTesteResp` int(11) NOT NULL,
  `CodQuestao` int(11) NOT NULL,
  `CodResposta` enum('A','B','C','D') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pre_teste_resposta`
--

INSERT INTO `pre_teste_resposta` (`CodTesteResp`, `CodQuestao`, `CodResposta`) VALUES
(1, 1, 'C'),
(2, 2, 'B'),
(3, 3, 'D'),
(4, 4, 'C'),
(5, 5, 'C'),
(6, 6, 'A'),
(7, 7, 'B'),
(8, 8, 'D'),
(9, 9, 'C'),
(10, 10, 'D');

-- --------------------------------------------------------

--
-- Estrutura da tabela `questionario_questao`
--

CREATE TABLE `questionario_questao` (
  `CodQuestao` int(11) NOT NULL,
  `descricao` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `questionario_questao`
--

INSERT INTO `questionario_questao` (`CodQuestao`, `descricao`) VALUES
(1, 'Você já utilizou algum Objeto de aprendizagem(OA)?Se sim qual?'),
(2, 'Você acredita que um Objeto de aprendizagem poderia contribuir para o ensino de SQL? Por que?\r\n'),
(3, 'Qual o seu nível de Experiência em SQL? Básico,Intermediário ou Avançado?'),
(4, 'Qual a sua opinião em relação a Interface gráfica e a usabilidade do OA?\r\n'),
(5, 'Você teve alguma dificuldade em utilizar o Objeto de aprendizagem? Se sim qual?\r\n'),
(6, 'Qual a sua avaliação sobre o OA(Considere-se em uma situação de estudo)?\r\n');

-- --------------------------------------------------------

--
-- Estrutura da tabela `questionario_resposta`
--

CREATE TABLE `questionario_resposta` (
  `CodResposta` int(11) NOT NULL,
  `CodQuestao` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `data` date NOT NULL,
  `descricaoResposta` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `turma`
--

CREATE TABLE `turma` (
  `CodTurma` int(11) NOT NULL,
  `CodDisciplina` int(11) DEFAULT NULL,
  `Semestre` varchar(25) DEFAULT NULL,
  `Ano` decimal(8,0) DEFAULT NULL,
  `Professor` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `turma`
--

INSERT INTO `turma` (`CodTurma`, `CodDisciplina`, `Semestre`, `Ano`, `Professor`) VALUES
(1, 1, '10', '2016', 'Jorge'),
(2, 2, '10', '2016', 'Professor2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countError` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `preteste` int(1) DEFAULT '0',
  `situacao` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `countError`, `created_at`, `updated_at`, `preteste`, `situacao`, `nota`) VALUES
(3, 'Thiago', 'thiagocardosooti@gmail.com', '$2y$10$ODPJhQWYVnc6M6hN3F5zCOlQm3kVszI/0/nw34NT98lH9x8rQD86a', '8uFWgNvLayIpywxZD5sBLVYeE4YLJNTVRTVWBm37qoGuwLim0CbzZUBhUvaJ', 0, '2016-12-28 02:32:19', '2017-05-07 01:24:57', 1, 'R', 6),
(4, 'Diego', 'teste@teste.com.br', '$2y$10$GOgLMhgnrzeNzwn/ZsXdruWyXfk87rKCo8a/tEtCih1/oSRHoUe5i', 'xHDPzeZ0AL80TDIa0TzQ4XUc37zF837ikGo5IZtGNeLyBNtpqggQWfC20Btv', NULL, '2017-04-21 19:10:05', '2017-04-21 19:10:09', 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_email_unique` (`email`);

--
-- Indexes for table `aluno`
--
ALTER TABLE `aluno`
  ADD PRIMARY KEY (`CodAluno`);

--
-- Indexes for table `disciplina`
--
ALTER TABLE `disciplina`
  ADD PRIMARY KEY (`CodDisciplina`);

--
-- Indexes for table `exercicios`
--
ALTER TABLE `exercicios`
  ADD PRIMARY KEY (`idExercicio`);

--
-- Indexes for table `historicoescolar`
--
ALTER TABLE `historicoescolar`
  ADD KEY `FK_Aluno_HistoricoEscolar` (`CodAluno`),
  ADD KEY `FK_Turma_HistoricoEscolar` (`CodTurma`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `prerequisito`
--
ALTER TABLE `prerequisito`
  ADD PRIMARY KEY (`CodPreRequisito`),
  ADD KEY `FK_Disciplina_PreRequisito` (`CodDisciplina`);

--
-- Indexes for table `pre_teste`
--
ALTER TABLE `pre_teste`
  ADD PRIMARY KEY (`CodPreteste`);

--
-- Indexes for table `pre_teste_opcoes`
--
ALTER TABLE `pre_teste_opcoes`
  ADD PRIMARY KEY (`CodPretesteOp`);

--
-- Indexes for table `pre_teste_resposta`
--
ALTER TABLE `pre_teste_resposta`
  ADD PRIMARY KEY (`CodTesteResp`);

--
-- Indexes for table `questionario_questao`
--
ALTER TABLE `questionario_questao`
  ADD PRIMARY KEY (`CodQuestao`);

--
-- Indexes for table `questionario_resposta`
--
ALTER TABLE `questionario_resposta`
  ADD PRIMARY KEY (`CodResposta`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`CodTurma`),
  ADD KEY `FK_Disciplina_Turma` (`CodDisciplina`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exercicios`
--
ALTER TABLE `exercicios`
  MODIFY `idExercicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pre_teste`
--
ALTER TABLE `pre_teste`
  MODIFY `CodPreteste` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pre_teste_opcoes`
--
ALTER TABLE `pre_teste_opcoes`
  MODIFY `CodPretesteOp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pre_teste_resposta`
--
ALTER TABLE `pre_teste_resposta`
  MODIFY `CodTesteResp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `questionario_questao`
--
ALTER TABLE `questionario_questao`
  MODIFY `CodQuestao` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questionario_resposta`
--
ALTER TABLE `questionario_resposta`
  MODIFY `CodResposta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `historicoescolar`
--
ALTER TABLE `historicoescolar`
  ADD CONSTRAINT `FK_Aluno_HistoricoEscolar` FOREIGN KEY (`CodAluno`) REFERENCES `aluno` (`CodAluno`),
  ADD CONSTRAINT `FK_Turma_HistoricoEscolar` FOREIGN KEY (`CodTurma`) REFERENCES `turma` (`CodTurma`);

--
-- Limitadores para a tabela `prerequisito`
--
ALTER TABLE `prerequisito`
  ADD CONSTRAINT `FK_Disciplina_PreRequisito` FOREIGN KEY (`CodDisciplina`) REFERENCES `disciplina` (`CodDisciplina`);

--
-- Limitadores para a tabela `turma`
--
ALTER TABLE `turma`
  ADD CONSTRAINT `FK_Disciplina_Turma` FOREIGN KEY (`CodDisciplina`) REFERENCES `disciplina` (`CodDisciplina`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
